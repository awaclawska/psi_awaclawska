/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Sterownik.h
*********************************************************************/

#ifndef Sterownik_H
#define Sterownik_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## link itsDetektor
#include "Detektor.h"
//## link itsLampa
#include "Lampa.h"
//## link itsNaped
#include "Naped.h"
//## link itsPrzycisk
class Przycisk;

//## link itsZmywarka
class Zmywarka;

//## package Default

//## class Sterownik
class Sterownik : public OMThread, public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedSterownik;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Sterownik(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Sterownik();
    
    ////    Operations    ////
    
    //## operation ustawStan(Stany)
    void ustawStan(const Stany& stan);
    
    ////    Additional operations    ////
    
    //## auto_generated
    void setPozycja(float p_pozycja);
    
    //## auto_generated
    int getStan() const;
    
    //## auto_generated
    void setStan(int p_stan);
    
    //## auto_generated
    void setSygnalizacja(bool p_sygnalizacja);
    
    //## auto_generated
    Detektor* getItsDetektor() const;
    
    //## auto_generated
    Lampa* getItsLampa() const;
    
    //## auto_generated
    Naped* getItsNaped() const;
    
    //## auto_generated
    Przycisk* getItsPrzycisk() const;
    
    //## auto_generated
    void setItsPrzycisk(Przycisk* p_Przycisk);
    
    //## auto_generated
    Zmywarka* getItsZmywarka() const;
    
    //## auto_generated
    void setItsZmywarka(Zmywarka* p_Zmywarka);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);

private :

    //## auto_generated
    float getDroga() const;
    
    //## auto_generated
    void setDroga(float p_droga);
    
    //## auto_generated
    float getPozycja() const;
    
    //## auto_generated
    float getPozycjaMax() const;
    
    //## auto_generated
    void setPozycjaMax(float p_pozycjaMax);
    
    //## auto_generated
    float getPozycjaMin() const;
    
    //## auto_generated
    void setPozycjaMin(float p_pozycjaMin);
    
    //## auto_generated
    int getRejestrAwarii() const;
    
    //## auto_generated
    void setRejestrAwarii(int p_rejestrAwarii);
    
    //## auto_generated
    Stany getStan0() const;
    
    //## auto_generated
    void setStan0(Stany p_stan0);
    
    //## auto_generated
    bool getSygnalizacja() const;
    
    ////    Attributes    ////

protected :

    float droga;		//## attribute droga
    
    float pozycja;		//## attribute pozycja
    
    float pozycjaMax;		//## attribute pozycjaMax
    
    float pozycjaMin;		//## attribute pozycjaMin
    
    int rejestrAwarii;		//## attribute rejestrAwarii
    
    int stan;		//## attribute stan
    
    Stany stan0;		//## attribute stan0
    
    bool sygnalizacja;		//## attribute sygnalizacja
    
    ////    Relations and components    ////
    
    Detektor itsDetektor;		//## link itsDetektor
    
    Lampa itsLampa;		//## link itsLampa
    
    Naped itsNaped;		//## link itsNaped
    
    Przycisk* itsPrzycisk;		//## link itsPrzycisk
    
    Zmywarka* itsZmywarka;		//## link itsZmywarka
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsPrzycisk(Przycisk* p_Przycisk);
    
    //## auto_generated
    void _setItsPrzycisk(Przycisk* p_Przycisk);
    
    //## auto_generated
    void _clearItsPrzycisk();
    
    //## auto_generated
    void __setItsZmywarka(Zmywarka* p_Zmywarka);
    
    //## auto_generated
    void _setItsZmywarka(Zmywarka* p_Zmywarka);
    
    //## auto_generated
    void _clearItsZmywarka();
    
    //## auto_generated
    virtual void destroy();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    inline bool rootState_isCompleted();
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // terminationstate_2:
    //## statechart_method
    inline bool terminationstate_2_IN() const;
    
    // Suszenie:
    //## statechart_method
    inline bool Suszenie_IN() const;
    
    //## statechart_method
    inline bool Suszenie_isCompleted();
    
    //## statechart_method
    void Suszenie_entDef();
    
    //## statechart_method
    void Suszenie_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Suszenie_handleEvent();
    
    // terminationstate_13:
    //## statechart_method
    inline bool terminationstate_13_IN() const;
    
    // sendaction_12:
    //## statechart_method
    inline bool sendaction_12_IN() const;
    
    // sendaction_11:
    //## statechart_method
    inline bool sendaction_11_IN() const;
    
    // sendaction_10:
    //## statechart_method
    inline bool sendaction_10_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_10_handleEvent();
    
    // sendaction_17:
    //## statechart_method
    inline bool sendaction_17_IN() const;
    
    // sendaction_16:
    //## statechart_method
    inline bool sendaction_16_IN() const;
    
    // Oczekiwanie:
    //## statechart_method
    inline bool Oczekiwanie_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Oczekiwanie_handleEvent();
    
    // Mycie:
    //## statechart_method
    inline bool Mycie_IN() const;
    
    //## statechart_method
    inline bool Mycie_isCompleted();
    
    //## statechart_method
    void Mycie_entDef();
    
    //## statechart_method
    void Mycie_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Mycie_handleEvent();
    
    // terminationstate_9:
    //## statechart_method
    inline bool terminationstate_9_IN() const;
    
    // sendaction_8:
    //## statechart_method
    inline bool sendaction_8_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_8_handleEvent();
    
    // sendaction_7:
    //## statechart_method
    inline bool sendaction_7_IN() const;
    
    // sendaction_6:
    //## statechart_method
    inline bool sendaction_6_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_6_handleEvent();
    
    // KrokRuchu:
    //## statechart_method
    inline bool KrokRuchu_IN() const;
    
    // Inicjalizacja:
    //## statechart_method
    inline bool Inicjalizacja_IN() const;
    
    // Awaria:
    //## statechart_method
    inline bool Awaria_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Sterownik_Enum {
        OMNonState = 0,
        terminationstate_2 = 1,
        Suszenie = 2,
        terminationstate_13 = 3,
        sendaction_12 = 4,
        sendaction_11 = 5,
        sendaction_10 = 6,
        sendaction_17 = 7,
        sendaction_16 = 8,
        Oczekiwanie = 9,
        Mycie = 10,
        terminationstate_9 = 11,
        sendaction_8 = 12,
        sendaction_7 = 13,
        sendaction_6 = 14,
        KrokRuchu = 15,
        Inicjalizacja = 16,
        Awaria = 17
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    int Suszenie_subState;
    
    int Mycie_subState;
    
    IOxfTimeout* rootState_timeout;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedSterownik : virtual public AOMInstance {
    DECLARE_REACTIVE_META(Sterownik, OMAnimatedSterownik)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_2_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Suszenie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_13_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_12_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_11_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_10_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_17_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_16_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Oczekiwanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Mycie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_9_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_8_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_7_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_6_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void KrokRuchu_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Inicjalizacja_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Awaria_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Sterownik::rootState_IN() const {
    return true;
}

inline bool Sterownik::rootState_isCompleted() {
    return ( IS_IN(terminationstate_2) );
}

inline bool Sterownik::terminationstate_2_IN() const {
    return rootState_subState == terminationstate_2;
}

inline bool Sterownik::Suszenie_IN() const {
    return rootState_subState == Suszenie;
}

inline bool Sterownik::Suszenie_isCompleted() {
    return ( IS_IN(terminationstate_13) );
}

inline bool Sterownik::terminationstate_13_IN() const {
    return Suszenie_subState == terminationstate_13;
}

inline bool Sterownik::sendaction_12_IN() const {
    return Suszenie_subState == sendaction_12;
}

inline bool Sterownik::sendaction_11_IN() const {
    return Suszenie_subState == sendaction_11;
}

inline bool Sterownik::sendaction_10_IN() const {
    return Suszenie_subState == sendaction_10;
}

inline bool Sterownik::sendaction_17_IN() const {
    return rootState_subState == sendaction_17;
}

inline bool Sterownik::sendaction_16_IN() const {
    return rootState_subState == sendaction_16;
}

inline bool Sterownik::Oczekiwanie_IN() const {
    return rootState_subState == Oczekiwanie;
}

inline bool Sterownik::Mycie_IN() const {
    return rootState_subState == Mycie;
}

inline bool Sterownik::Mycie_isCompleted() {
    return ( IS_IN(terminationstate_9) );
}

inline bool Sterownik::terminationstate_9_IN() const {
    return Mycie_subState == terminationstate_9;
}

inline bool Sterownik::sendaction_8_IN() const {
    return Mycie_subState == sendaction_8;
}

inline bool Sterownik::sendaction_7_IN() const {
    return Mycie_subState == sendaction_7;
}

inline bool Sterownik::sendaction_6_IN() const {
    return Mycie_subState == sendaction_6;
}

inline bool Sterownik::KrokRuchu_IN() const {
    return rootState_subState == KrokRuchu;
}

inline bool Sterownik::Inicjalizacja_IN() const {
    return rootState_subState == Inicjalizacja;
}

inline bool Sterownik::Awaria_IN() const {
    return rootState_subState == Awaria;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sterownik.h
*********************************************************************/
