/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Lampa
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Lampa.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Lampa.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_Lampa_Lampa_SERIALIZE OM_NO_OP

#define Default_Lampa_czytajUstawienia_SERIALIZE OM_NO_OP

#define Default_Lampa_zapiszUstawienia_SERIALIZE aomsmethod->addAttribute("nastawy", UNKNOWN2STRING(nastawy));
//#]

//## package Default

//## class Lampa
Lampa::Lampa(IOxfActive* theActiveContext) : swieciCzerwony(false), swieciZielony(false) {
    NOTIFY_ACTIVE_CONSTRUCTOR(Lampa, Lampa(), 0, Default_Lampa_Lampa_SERIALIZE);
    setActiveContext(this, true);
    itsSterownik = NULL;
    initStatechart();
}

Lampa::~Lampa() {
    NOTIFY_DESTRUCTOR(~Lampa, false);
    cleanUpRelations();
}

std::string Lampa::czytajUstawienia() {
    NOTIFY_OPERATION(czytajUstawienia, czytajUstawienia(), 0, Default_Lampa_czytajUstawienia_SERIALIZE);
    //#[ operation czytajUstawienia()
    //#]
}

bool Lampa::zapiszUstawienia(std::string nastawy) {
    NOTIFY_OPERATION(zapiszUstawienia, zapiszUstawienia(std::string), 1, Default_Lampa_zapiszUstawienia_SERIALIZE);
    //#[ operation zapiszUstawienia(std::string)
    //#]
}

void Lampa::setSwieciCzerwony(bool p_swieciCzerwony) {
    swieciCzerwony = p_swieciCzerwony;
    NOTIFY_SET_OPERATION;
}

void Lampa::setSwieciZielony(bool p_swieciZielony) {
    swieciZielony = p_swieciZielony;
    NOTIFY_SET_OPERATION;
}

Sterownik* Lampa::getItsSterownik() const {
    return itsSterownik;
}

void Lampa::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Lampa::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void Lampa::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Lampa::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

float Lampa::getDroga() const {
    return droga;
}

void Lampa::setDroga(float p_droga) {
    droga = p_droga;
}

Stany Lampa::getKierunek() const {
    return kierunek;
}

void Lampa::setKierunek(Stany p_kierunek) {
    kierunek = p_kierunek;
}

bool Lampa::getSwieciCzerwony() const {
    return swieciCzerwony;
}

bool Lampa::getSwieciZielony() const {
    return swieciZielony;
}

void Lampa::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Lampa::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Lampa::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

void Lampa::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Lampa::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(evStart_Default_id))
                {
                    OMSETPARAMS(evStart);
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda");
                    rootState_subState = Jazda;
                    rootState_active = Jazda;
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Jazda
        case Jazda:
        {
            if(IS_EVENT_TYPE_OF(evStop_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("7");
                    NOTIFY_STATE_EXITED("ROOT.Jazda");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
                    rootState_subState = Wylaczony;
                    rootState_active = Wylaczony;
                    NOTIFY_TRANSITION_TERMINATED("7");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evEtap_Default_id))
                {
                    OMSETPARAMS(evEtap);
                    //## transition 3 
                    if(params->droga < 0.)
                        {
                            NOTIFY_TRANSITION_STARTED("2");
                            NOTIFY_TRANSITION_STARTED("3");
                            NOTIFY_STATE_EXITED("ROOT.Jazda");
                            NOTIFY_STATE_ENTERED("ROOT.Otwieranie");
                            pushNullTransition();
                            rootState_subState = Otwieranie;
                            rootState_active = Otwieranie;
                            //#[ state Otwieranie.(Entry) 
                            if(swieciZielony)
                            	swieciZielony = false;
                            	else
                            	swieciZielony = true;
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("3");
                            NOTIFY_TRANSITION_TERMINATED("2");
                            res = eventConsumed;
                        }
                    else
                        {
                            //## transition 4 
                            if(params->droga > 0.)
                                {
                                    NOTIFY_TRANSITION_STARTED("2");
                                    NOTIFY_TRANSITION_STARTED("4");
                                    NOTIFY_STATE_EXITED("ROOT.Jazda");
                                    NOTIFY_STATE_ENTERED("ROOT.Zamykanie");
                                    pushNullTransition();
                                    rootState_subState = Zamykanie;
                                    rootState_active = Zamykanie;
                                    //#[ state Zamykanie.(Entry) 
                                    if(swieciCzerwony)
                                    	swieciCzerwony = false;
                                    	else
                                    	swieciCzerwony = true;
                                    //#]
                                    NOTIFY_TRANSITION_TERMINATED("4");
                                    NOTIFY_TRANSITION_TERMINATED("2");
                                    res = eventConsumed;
                                }
                        }
                }
            
        }
        break;
        // State Otwieranie
        case Otwieranie:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Otwieranie");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda");
                    rootState_subState = Jazda;
                    rootState_active = Jazda;
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Zamykanie
        case Zamykanie:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("6");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Zamykanie");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda");
                    rootState_subState = Jazda;
                    rootState_active = Jazda;
                    NOTIFY_TRANSITION_TERMINATED("6");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedLampa::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("kierunek", x2String((int)myReal->kierunek));
    aomsAttributes->addAttribute("swieciZielony", x2String(myReal->swieciZielony));
    aomsAttributes->addAttribute("swieciCzerwony", x2String(myReal->swieciCzerwony));
    aomsAttributes->addAttribute("droga", x2String(myReal->droga));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedLampa::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedLampa::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Lampa::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case Lampa::Jazda:
        {
            Jazda_serializeStates(aomsState);
        }
        break;
        case Lampa::Otwieranie:
        {
            Otwieranie_serializeStates(aomsState);
        }
        break;
        case Lampa::Zamykanie:
        {
            Zamykanie_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedLampa::Zamykanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Zamykanie");
}

void OMAnimatedLampa::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedLampa::Otwieranie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Otwieranie");
}

void OMAnimatedLampa::Jazda_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Lampa, Default, false, Modul, OMAnimatedModul, OMAnimatedLampa)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Lampa.cpp
*********************************************************************/
