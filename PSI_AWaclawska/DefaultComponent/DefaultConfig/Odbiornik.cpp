/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Odbiornik
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Odbiornik.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Odbiornik.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_Odbiornik_Odbiornik_SERIALIZE OM_NO_OP

#define Default_Odbiornik_Dekodowanie_SERIALIZE OM_NO_OP

#define Default_Odbiornik_czytajUstawienia_SERIALIZE OM_NO_OP

#define Default_Odbiornik_zapiszUstawienia_SERIALIZE aomsmethod->addAttribute("nastawy", UNKNOWN2STRING(nastawy));
//#]

//## package Default

//## class Odbiornik
Odbiornik::Odbiornik(IOxfActive* theActiveContext) {
    NOTIFY_ACTIVE_CONSTRUCTOR(Odbiornik, Odbiornik(), 0, Default_Odbiornik_Odbiornik_SERIALIZE);
    setActiveContext(this, true);
    {
        {
            itsPrzycisk.setShouldDelete(false);
        }
    }
    itsSterownik = NULL;
    initRelations();
    initStatechart();
}

Odbiornik::~Odbiornik() {
    NOTIFY_DESTRUCTOR(~Odbiornik, false);
    cleanUpRelations();
}

void Odbiornik::Dekodowanie() {
    NOTIFY_OPERATION(Dekodowanie, Dekodowanie(), 0, Default_Odbiornik_Dekodowanie_SERIALIZE);
    //#[ operation Dekodowanie()
    impulsOK = true;
    //#]
}

std::string Odbiornik::czytajUstawienia() {
    NOTIFY_OPERATION(czytajUstawienia, czytajUstawienia(), 0, Default_Odbiornik_czytajUstawienia_SERIALIZE);
    //#[ operation czytajUstawienia()
    //#]
}

bool Odbiornik::zapiszUstawienia(std::string nastawy) {
    NOTIFY_OPERATION(zapiszUstawienia, zapiszUstawienia(std::string), 1, Default_Odbiornik_zapiszUstawienia_SERIALIZE);
    //#[ operation zapiszUstawienia(std::string)
    //#]
}

Przycisk* Odbiornik::getItsPrzycisk() const {
    return (Przycisk*) &itsPrzycisk;
}

Sterownik* Odbiornik::getItsSterownik() const {
    return itsSterownik;
}

void Odbiornik::setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

bool Odbiornik::startBehavior() {
    bool done = true;
    done &= itsPrzycisk.startBehavior();
    done &= OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void Odbiornik::initRelations() {
    itsPrzycisk._setItsOdbiornik(this);
}

void Odbiornik::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Odbiornik::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

bool Odbiornik::getImpulsOK() const {
    return impulsOK;
}

void Odbiornik::setImpulsOK(bool p_impulsOK) {
    impulsOK = p_impulsOK;
}

void Odbiornik::destroy() {
    itsPrzycisk.destroy();
    OMReactive::destroy();
}

void Odbiornik::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Odbiornik::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(evZamkniete_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("6");
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    NOTIFY_STATE_ENTERED("ROOT.sendaction_4");
                    pushNullTransition();
                    rootState_subState = sendaction_4;
                    rootState_active = sendaction_4;
                    //#[ state sendaction_4.(Entry) 
                    itsPrzycisk.GEN(evZamkniete);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("6");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Oczekiwanie
        case Oczekiwanie:
        {
            if(IS_EVENT_TYPE_OF(evZamkniete_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
                    NOTIFY_STATE_ENTERED("ROOT.sendaction_5");
                    pushNullTransition();
                    rootState_subState = sendaction_5;
                    rootState_active = sendaction_5;
                    //#[ state sendaction_5.(Entry) 
                    itsPrzycisk.GEN(evZamkniete);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evPrzycisk_Default_id))
                {
                    //## transition 3 
                    if(impulsOK!=true)
                        {
                            NOTIFY_TRANSITION_STARTED("1");
                            NOTIFY_TRANSITION_STARTED("3");
                            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
                            //#[ transition 1 
                             Dekodowanie();
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            NOTIFY_TRANSITION_TERMINATED("3");
                            NOTIFY_TRANSITION_TERMINATED("1");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("1");
                            NOTIFY_TRANSITION_STARTED("2");
                            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
                            //#[ transition 1 
                             Dekodowanie();
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_3");
                            pushNullTransition();
                            rootState_subState = sendaction_3;
                            rootState_active = sendaction_3;
                            //#[ state sendaction_3.(Entry) 
                            itsSterownik->GEN(evPrzycisk);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("2");
                            NOTIFY_TRANSITION_TERMINATED("1");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_3
        case sendaction_3:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("4");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_3");
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    NOTIFY_TRANSITION_TERMINATED("4");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_4
        case sendaction_4:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("7");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_4");
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    NOTIFY_TRANSITION_TERMINATED("7");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_5
        case sendaction_5:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("8");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_5");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
                    rootState_subState = Wylaczony;
                    rootState_active = Wylaczony;
                    NOTIFY_TRANSITION_TERMINATED("8");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedOdbiornik::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("impulsOK", x2String(myReal->impulsOK));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedOdbiornik::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    aomsRelations->addRelation("itsPrzycisk", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsPrzycisk);
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedOdbiornik::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Odbiornik::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case Odbiornik::Oczekiwanie:
        {
            Oczekiwanie_serializeStates(aomsState);
        }
        break;
        case Odbiornik::sendaction_3:
        {
            sendaction_3_serializeStates(aomsState);
        }
        break;
        case Odbiornik::sendaction_4:
        {
            sendaction_4_serializeStates(aomsState);
        }
        break;
        case Odbiornik::sendaction_5:
        {
            sendaction_5_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedOdbiornik::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedOdbiornik::sendaction_5_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_5");
}

void OMAnimatedOdbiornik::sendaction_4_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_4");
}

void OMAnimatedOdbiornik::sendaction_3_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_3");
}

void OMAnimatedOdbiornik::Oczekiwanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Oczekiwanie");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Odbiornik, Default, false, Modul, OMAnimatedModul, OMAnimatedOdbiornik)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Odbiornik.cpp
*********************************************************************/
