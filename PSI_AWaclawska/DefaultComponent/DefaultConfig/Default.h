/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/

#ifndef Default_H
#define Default_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include <oxf/event.h>
//## auto_generated
class Detektor;

//## auto_generated
class Lampa;

//## auto_generated
class Modul;

//## auto_generated
class Naped;

//## auto_generated
class Odbiornik;

//## auto_generated
class Przycisk;

//## auto_generated
class Sterownik;

//## auto_generated
class Ustawienia;

//## auto_generated
class Zmywarka;

//#[ ignore
#define eventmessage_0_Default_id 18601

#define evPrzycisk_Default_id 18602

#define evValidate_Default_id 18603

#define evDekoduj_Default_id 18604

#define evStop_Default_id 18605

#define evStart_Default_id 18606

#define evEtap_Default_id 18607

#define evMigaj_Default_id 18608

#define evSprawdzKolizje_Default_id 18609

#define evKolizja_Default_id 18610

#define evPauza_Default_id 18611

#define evZamkniete_Default_id 18612

#define evTestowanie_Default_id 18613

#define evZatrzymanie_Default_id 18614

#define evSuszenie_Default_id 18615
//#]

//## package Default


//## type Stany
enum Stany {
    ZAMYKANIE = 1,
    ZATRZYMANIE = 0,
    OTWIERANIE = -1
};

//## event eventmessage_0()
class eventmessage_0 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedeventmessage_0;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    eventmessage_0();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedeventmessage_0 : virtual public AOMEvent {
    DECLARE_META_EVENT(eventmessage_0)
};
//#]
#endif // _OMINSTRUMENT

//## event evPrzycisk()
class evPrzycisk : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevPrzycisk;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evPrzycisk();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevPrzycisk : virtual public AOMEvent {
    DECLARE_META_EVENT(evPrzycisk)
};
//#]
#endif // _OMINSTRUMENT

//## event evValidate()
class evValidate : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevValidate;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evValidate();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevValidate : virtual public AOMEvent {
    DECLARE_META_EVENT(evValidate)
};
//#]
#endif // _OMINSTRUMENT

//## event evDekoduj()
class evDekoduj : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevDekoduj;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evDekoduj();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevDekoduj : virtual public AOMEvent {
    DECLARE_META_EVENT(evDekoduj)
};
//#]
#endif // _OMINSTRUMENT

//## event evStop()
class evStop : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStop;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evStop();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStop : virtual public AOMEvent {
    DECLARE_META_EVENT(evStop)
};
//#]
#endif // _OMINSTRUMENT

//## event evStart(Stany)
class evStart : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStart;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
//#[ ignore
    evStart(int p_stan);
//#]

    //## auto_generated
    evStart();
    
    //## auto_generated
    evStart(Stany p_stan);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
    
    ////    Framework    ////
    
    Stany stan;		//## auto_generated
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStart : virtual public AOMEvent {
    DECLARE_META_EVENT(evStart)
};
//#]
#endif // _OMINSTRUMENT

//## event evEtap(float)
class evEtap : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevEtap;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evEtap();
    
    //## auto_generated
    evEtap(float p_droga);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
    
    ////    Framework    ////
    
    float droga;		//## auto_generated
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevEtap : virtual public AOMEvent {
    DECLARE_META_EVENT(evEtap)
};
//#]
#endif // _OMINSTRUMENT

//## event evMigaj()
class evMigaj : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevMigaj;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evMigaj();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevMigaj : virtual public AOMEvent {
    DECLARE_META_EVENT(evMigaj)
};
//#]
#endif // _OMINSTRUMENT

//## event evSprawdzKolizje()
class evSprawdzKolizje : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevSprawdzKolizje;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evSprawdzKolizje();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevSprawdzKolizje : virtual public AOMEvent {
    DECLARE_META_EVENT(evSprawdzKolizje)
};
//#]
#endif // _OMINSTRUMENT

//## event evKolizja()
class evKolizja : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKolizja;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKolizja();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKolizja : virtual public AOMEvent {
    DECLARE_META_EVENT(evKolizja)
};
//#]
#endif // _OMINSTRUMENT

//## event evPauza()
class evPauza : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevPauza;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evPauza();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevPauza : virtual public AOMEvent {
    DECLARE_META_EVENT(evPauza)
};
//#]
#endif // _OMINSTRUMENT

//## event evZamkniete()
class evZamkniete : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevZamkniete;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evZamkniete();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevZamkniete : virtual public AOMEvent {
    DECLARE_META_EVENT(evZamkniete)
};
//#]
#endif // _OMINSTRUMENT

//## event evTestowanie()
class evTestowanie : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevTestowanie;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evTestowanie();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevTestowanie : virtual public AOMEvent {
    DECLARE_META_EVENT(evTestowanie)
};
//#]
#endif // _OMINSTRUMENT

//## event evZatrzymanie()
class evZatrzymanie : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevZatrzymanie;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evZatrzymanie();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevZatrzymanie : virtual public AOMEvent {
    DECLARE_META_EVENT(evZatrzymanie)
};
//#]
#endif // _OMINSTRUMENT

//## event evSuszenie()
class evSuszenie : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevSuszenie;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evSuszenie();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevSuszenie : virtual public AOMEvent {
    DECLARE_META_EVENT(evSuszenie)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/
