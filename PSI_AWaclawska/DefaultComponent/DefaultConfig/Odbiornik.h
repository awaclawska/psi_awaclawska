/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Odbiornik
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Odbiornik.h
*********************************************************************/

#ifndef Odbiornik_H
#define Odbiornik_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Odbiornik
#include "Modul.h"
//## link itsPrzycisk
#include "Przycisk.h"
//## link itsSterownik
class Sterownik;

//## package Default

//## class Odbiornik
class Odbiornik : public OMThread, public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedOdbiornik;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Odbiornik(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Odbiornik();
    
    ////    Operations    ////
    
    //## operation Dekodowanie()
    void Dekodowanie();
    
    //## operation czytajUstawienia()
    virtual std::string czytajUstawienia();
    
    //## operation zapiszUstawienia(std::string)
    virtual bool zapiszUstawienia(std::string nastawy);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Przycisk* getItsPrzycisk() const;
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    bool getImpulsOK() const;
    
    //## auto_generated
    void setImpulsOK(bool p_impulsOK);
    
    ////    Attributes    ////

protected :

    bool impulsOK;		//## attribute impulsOK
    
    ////    Relations and components    ////
    
    Przycisk itsPrzycisk;		//## link itsPrzycisk
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    virtual void destroy();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // sendaction_5:
    //## statechart_method
    inline bool sendaction_5_IN() const;
    
    // sendaction_4:
    //## statechart_method
    inline bool sendaction_4_IN() const;
    
    // sendaction_3:
    //## statechart_method
    inline bool sendaction_3_IN() const;
    
    // Oczekiwanie:
    //## statechart_method
    inline bool Oczekiwanie_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Odbiornik_Enum {
        OMNonState = 0,
        Wylaczony = 1,
        sendaction_5 = 2,
        sendaction_4 = 3,
        sendaction_3 = 4,
        Oczekiwanie = 5
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedOdbiornik : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Odbiornik, OMAnimatedOdbiornik)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_5_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_4_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_3_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Oczekiwanie_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Odbiornik::rootState_IN() const {
    return true;
}

inline bool Odbiornik::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool Odbiornik::sendaction_5_IN() const {
    return rootState_subState == sendaction_5;
}

inline bool Odbiornik::sendaction_4_IN() const {
    return rootState_subState == sendaction_4;
}

inline bool Odbiornik::sendaction_3_IN() const {
    return rootState_subState == sendaction_3;
}

inline bool Odbiornik::Oczekiwanie_IN() const {
    return rootState_subState == Oczekiwanie;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Odbiornik.h
*********************************************************************/
