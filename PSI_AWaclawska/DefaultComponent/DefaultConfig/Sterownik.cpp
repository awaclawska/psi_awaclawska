/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Sterownik.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Sterownik.h"
//## link itsPrzycisk
#include "Przycisk.h"
//## link itsZmywarka
#include "Zmywarka.h"
//#[ ignore
#define Default_Sterownik_Sterownik_SERIALIZE OM_NO_OP

#define Default_Sterownik_ustawStan_SERIALIZE aomsmethod->addAttribute("stan", x2String((int)stan));
//#]

//## package Default

//## class Sterownik
Sterownik::Sterownik(IOxfActive* theActiveContext) : pozycja(0), pozycjaMax(4.), pozycjaMin(0.), rejestrAwarii(0), sygnalizacja(true) {
    NOTIFY_ACTIVE_CONSTRUCTOR(Sterownik, Sterownik(), 0, Default_Sterownik_Sterownik_SERIALIZE);
    setActiveContext(this, true);
    {
        {
            itsLampa.setShouldDelete(false);
        }
        {
            itsDetektor.setShouldDelete(false);
        }
        {
            itsNaped.setShouldDelete(false);
        }
    }
    itsPrzycisk = NULL;
    itsZmywarka = NULL;
    initRelations();
    initStatechart();
}

Sterownik::~Sterownik() {
    NOTIFY_DESTRUCTOR(~Sterownik, true);
    cleanUpRelations();
    cancelTimeouts();
}

void Sterownik::ustawStan(const Stany& stan) {
    NOTIFY_OPERATION(ustawStan, ustawStan(const Stany&), 1, Default_Sterownik_ustawStan_SERIALIZE);
    //#[ operation ustawStan(Stany)
    this->stan = stan;
    //#]
}

void Sterownik::setPozycja(float p_pozycja) {
    pozycja = p_pozycja;
    NOTIFY_SET_OPERATION;
}

int Sterownik::getStan() const {
    return stan;
}

void Sterownik::setStan(int p_stan) {
    stan = p_stan;
}

void Sterownik::setSygnalizacja(bool p_sygnalizacja) {
    sygnalizacja = p_sygnalizacja;
    NOTIFY_SET_OPERATION;
}

Detektor* Sterownik::getItsDetektor() const {
    return (Detektor*) &itsDetektor;
}

Lampa* Sterownik::getItsLampa() const {
    return (Lampa*) &itsLampa;
}

Naped* Sterownik::getItsNaped() const {
    return (Naped*) &itsNaped;
}

Przycisk* Sterownik::getItsPrzycisk() const {
    return itsPrzycisk;
}

void Sterownik::setItsPrzycisk(Przycisk* p_Przycisk) {
    _setItsPrzycisk(p_Przycisk);
}

Zmywarka* Sterownik::getItsZmywarka() const {
    return itsZmywarka;
}

void Sterownik::setItsZmywarka(Zmywarka* p_Zmywarka) {
    _setItsZmywarka(p_Zmywarka);
}

bool Sterownik::startBehavior() {
    bool done = true;
    done &= itsDetektor.startBehavior();
    done &= itsLampa.startBehavior();
    done &= itsNaped.startBehavior();
    done &= OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void Sterownik::initRelations() {
    itsDetektor._setItsSterownik(this);
    itsLampa._setItsSterownik(this);
    itsNaped._setItsSterownik(this);
}

void Sterownik::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    Suszenie_subState = OMNonState;
    Mycie_subState = OMNonState;
    rootState_timeout = NULL;
}

void Sterownik::cleanUpRelations() {
    if(itsPrzycisk != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsPrzycisk");
            itsPrzycisk = NULL;
        }
    if(itsZmywarka != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsZmywarka");
            itsZmywarka = NULL;
        }
}

void Sterownik::cancelTimeouts() {
    cancel(rootState_timeout);
}

bool Sterownik::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(rootState_timeout == arg)
        {
            rootState_timeout = NULL;
            res = true;
        }
    return res;
}

float Sterownik::getDroga() const {
    return droga;
}

void Sterownik::setDroga(float p_droga) {
    droga = p_droga;
}

float Sterownik::getPozycja() const {
    return pozycja;
}

float Sterownik::getPozycjaMax() const {
    return pozycjaMax;
}

void Sterownik::setPozycjaMax(float p_pozycjaMax) {
    pozycjaMax = p_pozycjaMax;
}

float Sterownik::getPozycjaMin() const {
    return pozycjaMin;
}

void Sterownik::setPozycjaMin(float p_pozycjaMin) {
    pozycjaMin = p_pozycjaMin;
}

int Sterownik::getRejestrAwarii() const {
    return rejestrAwarii;
}

void Sterownik::setRejestrAwarii(int p_rejestrAwarii) {
    rejestrAwarii = p_rejestrAwarii;
}

Stany Sterownik::getStan0() const {
    return stan0;
}

void Sterownik::setStan0(Stany p_stan0) {
    stan0 = p_stan0;
}

bool Sterownik::getSygnalizacja() const {
    return sygnalizacja;
}

void Sterownik::__setItsPrzycisk(Przycisk* p_Przycisk) {
    itsPrzycisk = p_Przycisk;
    if(p_Przycisk != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsPrzycisk", p_Przycisk, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsPrzycisk");
        }
}

void Sterownik::_setItsPrzycisk(Przycisk* p_Przycisk) {
    __setItsPrzycisk(p_Przycisk);
}

void Sterownik::_clearItsPrzycisk() {
    NOTIFY_RELATION_CLEARED("itsPrzycisk");
    itsPrzycisk = NULL;
}

void Sterownik::__setItsZmywarka(Zmywarka* p_Zmywarka) {
    itsZmywarka = p_Zmywarka;
    if(p_Zmywarka != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsZmywarka", p_Zmywarka, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsZmywarka");
        }
}

void Sterownik::_setItsZmywarka(Zmywarka* p_Zmywarka) {
    __setItsZmywarka(p_Zmywarka);
}

void Sterownik::_clearItsZmywarka() {
    NOTIFY_RELATION_CLEARED("itsZmywarka");
    itsZmywarka = NULL;
}

void Sterownik::destroy() {
    itsDetektor.destroy();
    itsLampa.destroy();
    itsNaped.destroy();
    OMReactive::destroy();
}

void Sterownik::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Inicjalizacja");
        pushNullTransition();
        rootState_subState = Inicjalizacja;
        rootState_active = Inicjalizacja;
        //#[ state Inicjalizacja.(Entry) 
        stan = ZATRZYMANIE;
        stan0 = ZAMYKANIE;
        //#]
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Sterownik::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Inicjalizacja
        case Inicjalizacja:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 1 
                    if(rejestrAwarii != 0)
                        {
                            NOTIFY_TRANSITION_STARTED("29");
                            NOTIFY_TRANSITION_STARTED("1");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Inicjalizacja");
                            NOTIFY_STATE_ENTERED("ROOT.Awaria");
                            rootState_subState = Awaria;
                            rootState_active = Awaria;
                            //#[ state Awaria.(Entry) 
                            std::cout << "\nAWARIA: " << rejestrAwarii;
                            //#]
                            rootState_timeout = scheduleTimeout(5000, "ROOT.Awaria");
                            NOTIFY_TRANSITION_TERMINATED("1");
                            NOTIFY_TRANSITION_TERMINATED("29");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("29");
                            NOTIFY_TRANSITION_STARTED("3");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Inicjalizacja");
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            NOTIFY_TRANSITION_TERMINATED("3");
                            NOTIFY_TRANSITION_TERMINATED("29");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State Awaria
        case Awaria:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("2");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.Awaria");
                            NOTIFY_STATE_ENTERED("ROOT.terminationstate_2");
                            rootState_subState = terminationstate_2;
                            rootState_active = terminationstate_2;
                            NOTIFY_TRANSITION_TERMINATED("2");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State Oczekiwanie
        case Oczekiwanie:
        {
            res = Oczekiwanie_handleEvent();
        }
        break;
        // State sendaction_6
        case sendaction_6:
        {
            res = sendaction_6_handleEvent();
        }
        break;
        // State sendaction_7
        case sendaction_7:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("22");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Mycie.sendaction_7");
                    NOTIFY_STATE_ENTERED("ROOT.Mycie.terminationstate_9");
                    Mycie_subState = terminationstate_9;
                    rootState_active = terminationstate_9;
                    NOTIFY_TRANSITION_TERMINATED("22");
                    res = eventConsumed;
                }
            
            if(res == eventNotConsumed)
                {
                    res = Mycie_handleEvent();
                }
        }
        break;
        // State sendaction_8
        case sendaction_8:
        {
            res = sendaction_8_handleEvent();
        }
        break;
        // State terminationstate_9
        case terminationstate_9:
        {
            res = Mycie_handleEvent();
        }
        break;
        // State sendaction_10
        case sendaction_10:
        {
            res = sendaction_10_handleEvent();
        }
        break;
        // State sendaction_11
        case sendaction_11:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("14");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Suszenie.sendaction_11");
                    NOTIFY_STATE_ENTERED("ROOT.Suszenie.terminationstate_13");
                    Suszenie_subState = terminationstate_13;
                    rootState_active = terminationstate_13;
                    NOTIFY_TRANSITION_TERMINATED("14");
                    res = eventConsumed;
                }
            
            if(res == eventNotConsumed)
                {
                    res = Suszenie_handleEvent();
                }
        }
        break;
        // State sendaction_12
        case sendaction_12:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("26");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Suszenie.sendaction_12");
                    NOTIFY_STATE_ENTERED("ROOT.Suszenie.sendaction_11");
                    pushNullTransition();
                    Suszenie_subState = sendaction_11;
                    rootState_active = sendaction_11;
                    //#[ state Suszenie.sendaction_11.(Entry) 
                    itsDetektor.GEN(evStop);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("26");
                    res = eventConsumed;
                }
            
            if(res == eventNotConsumed)
                {
                    res = Suszenie_handleEvent();
                }
        }
        break;
        // State terminationstate_13
        case terminationstate_13:
        {
            res = Suszenie_handleEvent();
        }
        break;
        // State KrokRuchu
        case KrokRuchu:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 17 
                    if(sygnalizacja==true)
                        {
                            NOTIFY_TRANSITION_STARTED("28");
                            NOTIFY_TRANSITION_STARTED("17");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.KrokRuchu");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_16");
                            pushNullTransition();
                            rootState_subState = sendaction_16;
                            rootState_active = sendaction_16;
                            //#[ state sendaction_16.(Entry) 
                            itsLampa.GEN(evEtap(droga));
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("17");
                            NOTIFY_TRANSITION_TERMINATED("28");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("28");
                            NOTIFY_TRANSITION_STARTED("16");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.KrokRuchu");
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            NOTIFY_TRANSITION_TERMINATED("16");
                            NOTIFY_TRANSITION_TERMINATED("28");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_16
        case sendaction_16:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("18");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_16");
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    NOTIFY_TRANSITION_TERMINATED("18");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_17
        case sendaction_17:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("20");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_17");
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    NOTIFY_TRANSITION_TERMINATED("20");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

void Sterownik::Suszenie_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Suszenie");
    pushNullTransition();
    rootState_subState = Suszenie;
    NOTIFY_TRANSITION_STARTED("10");
    NOTIFY_STATE_ENTERED("ROOT.Suszenie.sendaction_10");
    pushNullTransition();
    Suszenie_subState = sendaction_10;
    rootState_active = sendaction_10;
    //#[ state Suszenie.sendaction_10.(Entry) 
    itsNaped.GEN(evStop);
    //#]
    NOTIFY_TRANSITION_TERMINATED("10");
}

void Sterownik::Suszenie_exit() {
    popNullTransition();
    switch (Suszenie_subState) {
        // State sendaction_10
        case sendaction_10:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Suszenie.sendaction_10");
        }
        break;
        // State sendaction_11
        case sendaction_11:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Suszenie.sendaction_11");
        }
        break;
        // State sendaction_12
        case sendaction_12:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Suszenie.sendaction_12");
        }
        break;
        // State terminationstate_13
        case terminationstate_13:
        {
            NOTIFY_STATE_EXITED("ROOT.Suszenie.terminationstate_13");
        }
        break;
        default:
            break;
    }
    Suszenie_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Suszenie");
}

IOxfReactive::TakeEventStatus Sterownik::Suszenie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 9 
            if(IS_COMPLETED(Suszenie)==true)
                {
                    NOTIFY_TRANSITION_STARTED("9");
                    Suszenie_exit();
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    NOTIFY_TRANSITION_TERMINATED("9");
                    res = eventConsumed;
                }
        }
    
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_10_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 12 
            if(sygnalizacja == true)
                {
                    NOTIFY_TRANSITION_STARTED("11");
                    NOTIFY_TRANSITION_STARTED("12");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Suszenie.sendaction_10");
                    NOTIFY_STATE_ENTERED("ROOT.Suszenie.sendaction_12");
                    pushNullTransition();
                    Suszenie_subState = sendaction_12;
                    rootState_active = sendaction_12;
                    //#[ state Suszenie.sendaction_12.(Entry) 
                    itsLampa.GEN(evStop);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("12");
                    NOTIFY_TRANSITION_TERMINATED("11");
                    res = eventConsumed;
                }
            else
                {
                    NOTIFY_TRANSITION_STARTED("11");
                    NOTIFY_TRANSITION_STARTED("13");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Suszenie.sendaction_10");
                    NOTIFY_STATE_ENTERED("ROOT.Suszenie.sendaction_11");
                    pushNullTransition();
                    Suszenie_subState = sendaction_11;
                    rootState_active = sendaction_11;
                    //#[ state Suszenie.sendaction_11.(Entry) 
                    itsDetektor.GEN(evStop);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("13");
                    NOTIFY_TRANSITION_TERMINATED("11");
                    res = eventConsumed;
                }
        }
    
    if(res == eventNotConsumed)
        {
            res = Suszenie_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::Oczekiwanie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evMigaj_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("27");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 27 
            if (sygnalizacja == true)
            	sygnalizacja = false;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
            rootState_subState = Oczekiwanie;
            rootState_active = Oczekiwanie;
            NOTIFY_TRANSITION_TERMINATED("27");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evEtap_Default_id))
        {
            OMSETPARAMS(evEtap);
            NOTIFY_TRANSITION_STARTED("15");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.KrokRuchu");
            pushNullTransition();
            rootState_subState = KrokRuchu;
            rootState_active = KrokRuchu;
            //#[ state KrokRuchu.(Entry) 
            droga = params-> droga;
            pozycja += droga;
            std::cout << "\ndroga=" << params->droga 
            		<< ", pozycja=" << pozycja;
            if (pozycja >= pozycjaMax || pozycja <= pozycjaMin)
            	GEN(evZatrzymanie);
             itsDetektor.GEN(evSprawdzKolizje());
            //#]
            NOTIFY_TRANSITION_TERMINATED("15");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evZamkniete_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("19");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.sendaction_17");
            pushNullTransition();
            rootState_subState = sendaction_17;
            rootState_active = sendaction_17;
            //#[ state sendaction_17.(Entry) 
            itsPrzycisk->GEN(evZamkniete);
            //#]
            NOTIFY_TRANSITION_TERMINATED("19");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evPrzycisk_Default_id))
        {
            //## transition 5 
            if(stan == ZATRZYMANIE && stan0 == OTWIERANIE)
                {
                    NOTIFY_TRANSITION_STARTED("4");
                    NOTIFY_TRANSITION_STARTED("5");
                    NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
                    //#[ transition 5 
                    ustawStan(ZAMYKANIE);
                    //#]
                    Mycie_entDef();
                    NOTIFY_TRANSITION_TERMINATED("5");
                    NOTIFY_TRANSITION_TERMINATED("4");
                    res = eventConsumed;
                }
            else
                {
                    //## transition 6 
                    if(stan == ZATRZYMANIE && stan0 ==ZAMYKANIE)
                        {
                            NOTIFY_TRANSITION_STARTED("4");
                            NOTIFY_TRANSITION_STARTED("6");
                            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
                            //#[ transition 6 
                            ustawStan(OTWIERANIE);
                            //#]
                            Mycie_entDef();
                            NOTIFY_TRANSITION_TERMINATED("6");
                            NOTIFY_TRANSITION_TERMINATED("4");
                            res = eventConsumed;
                        }
                }
        }
    else if(IS_EVENT_TYPE_OF(evSuszenie_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("21");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            Suszenie_entDef();
            NOTIFY_TRANSITION_TERMINATED("21");
            res = eventConsumed;
        }
    
    return res;
}

void Sterownik::Mycie_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Mycie");
    pushNullTransition();
    rootState_subState = Mycie;
    NOTIFY_TRANSITION_STARTED("7");
    NOTIFY_STATE_ENTERED("ROOT.Mycie.sendaction_6");
    pushNullTransition();
    Mycie_subState = sendaction_6;
    rootState_active = sendaction_6;
    //#[ state Mycie.sendaction_6.(Entry) 
    itsNaped.GEN(evStart(stan));
    //#]
    NOTIFY_TRANSITION_TERMINATED("7");
}

void Sterownik::Mycie_exit() {
    popNullTransition();
    switch (Mycie_subState) {
        // State sendaction_6
        case sendaction_6:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Mycie.sendaction_6");
        }
        break;
        // State sendaction_7
        case sendaction_7:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Mycie.sendaction_7");
        }
        break;
        // State sendaction_8
        case sendaction_8:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Mycie.sendaction_8");
        }
        break;
        // State terminationstate_9
        case terminationstate_9:
        {
            NOTIFY_STATE_EXITED("ROOT.Mycie.terminationstate_9");
        }
        break;
        default:
            break;
    }
    Mycie_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Mycie");
}

IOxfReactive::TakeEventStatus Sterownik::Mycie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 30 
            if(IS_COMPLETED(Mycie)==true)
                {
                    NOTIFY_TRANSITION_STARTED("30");
                    Mycie_exit();
                    Suszenie_entDef();
                    NOTIFY_TRANSITION_TERMINATED("30");
                    res = eventConsumed;
                }
        }
    
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_8_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            NOTIFY_TRANSITION_STARTED("24");
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Mycie.sendaction_8");
            NOTIFY_STATE_ENTERED("ROOT.Mycie.sendaction_7");
            pushNullTransition();
            Mycie_subState = sendaction_7;
            rootState_active = sendaction_7;
            //#[ state Mycie.sendaction_7.(Entry) 
            itsDetektor.GEN(evStart(stan));
            //#]
            NOTIFY_TRANSITION_TERMINATED("24");
            res = eventConsumed;
        }
    
    if(res == eventNotConsumed)
        {
            res = Mycie_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_6_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 23 
            if(sygnalizacja==true)
                {
                    NOTIFY_TRANSITION_STARTED("8");
                    NOTIFY_TRANSITION_STARTED("23");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Mycie.sendaction_6");
                    NOTIFY_STATE_ENTERED("ROOT.Mycie.sendaction_8");
                    pushNullTransition();
                    Mycie_subState = sendaction_8;
                    rootState_active = sendaction_8;
                    //#[ state Mycie.sendaction_8.(Entry) 
                    itsLampa.GEN(evStart(stan));
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("23");
                    NOTIFY_TRANSITION_TERMINATED("8");
                    res = eventConsumed;
                }
            else
                {
                    NOTIFY_TRANSITION_STARTED("8");
                    NOTIFY_TRANSITION_STARTED("25");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Mycie.sendaction_6");
                    NOTIFY_STATE_ENTERED("ROOT.Mycie.sendaction_7");
                    pushNullTransition();
                    Mycie_subState = sendaction_7;
                    rootState_active = sendaction_7;
                    //#[ state Mycie.sendaction_7.(Entry) 
                    itsDetektor.GEN(evStart(stan));
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("25");
                    NOTIFY_TRANSITION_TERMINATED("8");
                    res = eventConsumed;
                }
        }
    
    if(res == eventNotConsumed)
        {
            res = Mycie_handleEvent();
        }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedSterownik::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("stan", x2String(myReal->stan));
    aomsAttributes->addAttribute("stan0", x2String((int)myReal->stan0));
    aomsAttributes->addAttribute("sygnalizacja", x2String(myReal->sygnalizacja));
    aomsAttributes->addAttribute("rejestrAwarii", x2String(myReal->rejestrAwarii));
    aomsAttributes->addAttribute("droga", x2String(myReal->droga));
    aomsAttributes->addAttribute("pozycja", x2String(myReal->pozycja));
    aomsAttributes->addAttribute("pozycjaMin", x2String(myReal->pozycjaMin));
    aomsAttributes->addAttribute("pozycjaMax", x2String(myReal->pozycjaMax));
}

void OMAnimatedSterownik::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsZmywarka", false, true);
    if(myReal->itsZmywarka)
        {
            aomsRelations->ADD_ITEM(myReal->itsZmywarka);
        }
    aomsRelations->addRelation("itsLampa", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsLampa);
    aomsRelations->addRelation("itsDetektor", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsDetektor);
    aomsRelations->addRelation("itsNaped", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsNaped);
    aomsRelations->addRelation("itsPrzycisk", false, true);
    if(myReal->itsPrzycisk)
        {
            aomsRelations->ADD_ITEM(myReal->itsPrzycisk);
        }
}

void OMAnimatedSterownik::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Sterownik::Inicjalizacja:
        {
            Inicjalizacja_serializeStates(aomsState);
        }
        break;
        case Sterownik::Awaria:
        {
            Awaria_serializeStates(aomsState);
        }
        break;
        case Sterownik::terminationstate_2:
        {
            terminationstate_2_serializeStates(aomsState);
        }
        break;
        case Sterownik::Oczekiwanie:
        {
            Oczekiwanie_serializeStates(aomsState);
        }
        break;
        case Sterownik::Mycie:
        {
            Mycie_serializeStates(aomsState);
        }
        break;
        case Sterownik::Suszenie:
        {
            Suszenie_serializeStates(aomsState);
        }
        break;
        case Sterownik::KrokRuchu:
        {
            KrokRuchu_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_16:
        {
            sendaction_16_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_17:
        {
            sendaction_17_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::terminationstate_2_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.terminationstate_2");
}

void OMAnimatedSterownik::Suszenie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Suszenie");
    switch (myReal->Suszenie_subState) {
        case Sterownik::sendaction_10:
        {
            sendaction_10_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_11:
        {
            sendaction_11_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_12:
        {
            sendaction_12_serializeStates(aomsState);
        }
        break;
        case Sterownik::terminationstate_13:
        {
            terminationstate_13_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::terminationstate_13_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Suszenie.terminationstate_13");
}

void OMAnimatedSterownik::sendaction_12_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Suszenie.sendaction_12");
}

void OMAnimatedSterownik::sendaction_11_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Suszenie.sendaction_11");
}

void OMAnimatedSterownik::sendaction_10_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Suszenie.sendaction_10");
}

void OMAnimatedSterownik::sendaction_17_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_17");
}

void OMAnimatedSterownik::sendaction_16_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_16");
}

void OMAnimatedSterownik::Oczekiwanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Oczekiwanie");
}

void OMAnimatedSterownik::Mycie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Mycie");
    switch (myReal->Mycie_subState) {
        case Sterownik::sendaction_6:
        {
            sendaction_6_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_7:
        {
            sendaction_7_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_8:
        {
            sendaction_8_serializeStates(aomsState);
        }
        break;
        case Sterownik::terminationstate_9:
        {
            terminationstate_9_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::terminationstate_9_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Mycie.terminationstate_9");
}

void OMAnimatedSterownik::sendaction_8_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Mycie.sendaction_8");
}

void OMAnimatedSterownik::sendaction_7_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Mycie.sendaction_7");
}

void OMAnimatedSterownik::sendaction_6_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Mycie.sendaction_6");
}

void OMAnimatedSterownik::KrokRuchu_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.KrokRuchu");
}

void OMAnimatedSterownik::Inicjalizacja_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Inicjalizacja");
}

void OMAnimatedSterownik::Awaria_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Awaria");
}
//#]

IMPLEMENT_REACTIVE_META_P(Sterownik, Default, Default, false, OMAnimatedSterownik)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sterownik.cpp
*********************************************************************/
