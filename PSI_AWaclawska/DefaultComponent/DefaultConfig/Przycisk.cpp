/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Przycisk
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Przycisk.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## link itsOdbiornik
#include "Odbiornik.h"
//## auto_generated
#include "Przycisk.h"
//#[ ignore
#define Default_Przycisk_Przycisk_SERIALIZE OM_NO_OP

#define Default_Przycisk_czytajUstawienia_SERIALIZE OM_NO_OP

#define Default_Przycisk_zapiszUstawienia_SERIALIZE aomsmethod->addAttribute("nastawy", UNKNOWN2STRING(nastawy));
//#]

//## package Default

//## class Przycisk
std::string Przycisk::czytajUstawienia() {
    NOTIFY_OPERATION(czytajUstawienia, czytajUstawienia(), 0, Default_Przycisk_czytajUstawienia_SERIALIZE);
    //#[ operation czytajUstawienia()
    return "";
    //#]
}

bool Przycisk::zapiszUstawienia(std::string nastawy) {
    NOTIFY_OPERATION(zapiszUstawienia, zapiszUstawienia(std::string), 1, Default_Przycisk_zapiszUstawienia_SERIALIZE);
    //#[ operation zapiszUstawienia(std::string)
    return true;
    //#]
}

Odbiornik* Przycisk::getItsOdbiornik() const {
    return itsOdbiornik;
}

void Przycisk::setItsOdbiornik(Odbiornik* p_Odbiornik) {
    _setItsOdbiornik(p_Odbiornik);
}

bool Przycisk::startBehavior() {
    bool done = true;
    done &= itsSterownik.startBehavior();
    done &= OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void Przycisk::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    rootState_timeout = NULL;
}

void Przycisk::cleanUpRelations() {
    if(itsOdbiornik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsOdbiornik");
            itsOdbiornik = NULL;
        }
    if(itsSterownik_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik_1");
            itsSterownik_1 = NULL;
        }
}

void Przycisk::cancelTimeouts() {
    cancel(rootState_timeout);
}

bool Przycisk::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(rootState_timeout == arg)
        {
            rootState_timeout = NULL;
            res = true;
        }
    return res;
}

void Przycisk::__setItsOdbiornik(Odbiornik* p_Odbiornik) {
    itsOdbiornik = p_Odbiornik;
    if(p_Odbiornik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsOdbiornik", p_Odbiornik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsOdbiornik");
        }
}

void Przycisk::_setItsOdbiornik(Odbiornik* p_Odbiornik) {
    __setItsOdbiornik(p_Odbiornik);
}

void Przycisk::_clearItsOdbiornik() {
    NOTIFY_RELATION_CLEARED("itsOdbiornik");
    itsOdbiornik = NULL;
}

Przycisk::Przycisk(IOxfActive* theActiveContext) {
    NOTIFY_ACTIVE_CONSTRUCTOR(Przycisk, Przycisk(), 0, Default_Przycisk_Przycisk_SERIALIZE);
    setActiveContext(this, true);
    {
        {
            itsSterownik.setShouldDelete(false);
        }
    }
    itsOdbiornik = NULL;
    itsSterownik_1 = NULL;
    initRelations();
    initStatechart();
}

Przycisk::~Przycisk() {
    NOTIFY_DESTRUCTOR(~Przycisk, false);
    cleanUpRelations();
    cancelTimeouts();
}

Sterownik* Przycisk::getItsSterownik() const {
    return (Sterownik*) &itsSterownik;
}

Sterownik* Przycisk::getItsSterownik_1() const {
    return itsSterownik_1;
}

void Przycisk::setItsSterownik_1(Sterownik* p_Sterownik) {
    itsSterownik_1 = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik_1", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik_1");
        }
}

void Przycisk::initRelations() {
    itsSterownik._setItsPrzycisk(this);
}

void Przycisk::destroy() {
    itsSterownik.destroy();
    OMReactive::destroy();
}

void Przycisk::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Przycisk::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(evZamkniete_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Oczekiwanie
        case Oczekiwanie:
        {
            if(IS_EVENT_TYPE_OF(evZamkniete_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("4");
                    NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
                    rootState_subState = Wylaczony;
                    rootState_active = Wylaczony;
                    NOTIFY_TRANSITION_TERMINATED("4");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evPrzycisk_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("2");
                    NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
                    NOTIFY_STATE_ENTERED("ROOT.sendaction_2");
                    rootState_subState = sendaction_2;
                    rootState_active = sendaction_2;
                    //#[ state sendaction_2.(Entry) 
                    itsSterownik.GEN(evPrzycisk);
                    //#]
                    rootState_timeout = scheduleTimeout(0, "ROOT.sendaction_2");
                    NOTIFY_TRANSITION_TERMINATED("2");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_2
        case sendaction_2:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("3");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.sendaction_2");
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            NOTIFY_TRANSITION_TERMINATED("3");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedPrzycisk::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedPrzycisk::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsOdbiornik", false, true);
    if(myReal->itsOdbiornik)
        {
            aomsRelations->ADD_ITEM(myReal->itsOdbiornik);
        }
    aomsRelations->addRelation("itsSterownik", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSterownik);
    aomsRelations->addRelation("itsSterownik_1", false, true);
    if(myReal->itsSterownik_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik_1);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedPrzycisk::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Przycisk::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case Przycisk::Oczekiwanie:
        {
            Oczekiwanie_serializeStates(aomsState);
        }
        break;
        case Przycisk::sendaction_2:
        {
            sendaction_2_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedPrzycisk::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedPrzycisk::sendaction_2_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_2");
}

void OMAnimatedPrzycisk::Oczekiwanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Oczekiwanie");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Przycisk, Default, false, Modul, OMAnimatedModul, OMAnimatedPrzycisk)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Przycisk.cpp
*********************************************************************/
