/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Detektor
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Detektor.h
*********************************************************************/

#ifndef Detektor_H
#define Detektor_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Detektor
#include "Modul.h"
//## link itsSterownik
class Sterownik;

//## package Default

//## class Detektor
class Detektor : public OMThread, public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedDetektor;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Detektor(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Detektor();
    
    ////    Operations    ////
    
    //## operation czytajUstawienia()
    virtual std::string czytajUstawienia();
    
    //## operation zapiszUstawienia(std::string)
    virtual bool zapiszUstawienia(std::string nastawy);

private :

    //## operation zerujIloscImpulsow()
    void zerujIloscImpulsow();
    
    ////    Additional operations    ////

public :

    //## auto_generated
    void setIloscImpulsowOdebranych(int p_iloscImpulsowOdebranych);
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    int getIloscImpulsowOdebranych() const;
    
    //## auto_generated
    int getIloscImpulsowWyslanych() const;
    
    //## auto_generated
    void setIloscImpulsowWyslanych(int p_iloscImpulsowWyslanych);
    
    //## auto_generated
    Stany getKierunek() const;
    
    //## auto_generated
    void setKierunek(Stany p_kierunek);
    
    ////    Attributes    ////

protected :

    int iloscImpulsowOdebranych;		//## attribute iloscImpulsowOdebranych
    
    int iloscImpulsowWyslanych;		//## attribute iloscImpulsowWyslanych
    
    Stany kierunek;		//## attribute kierunek
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // SprawdzanieKolizji:
    //## statechart_method
    inline bool SprawdzanieKolizji_IN() const;
    
    // sendaction_3:
    //## statechart_method
    inline bool sendaction_3_IN() const;
    
    // Jazda:
    //## statechart_method
    inline bool Jazda_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Detektor_Enum {
        OMNonState = 0,
        Wylaczony = 1,
        SprawdzanieKolizji = 2,
        sendaction_3 = 3,
        Jazda = 4
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedDetektor : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Detektor, OMAnimatedDetektor)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void SprawdzanieKolizji_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_3_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Jazda_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Detektor::rootState_IN() const {
    return true;
}

inline bool Detektor::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool Detektor::SprawdzanieKolizji_IN() const {
    return rootState_subState == SprawdzanieKolizji;
}

inline bool Detektor::sendaction_3_IN() const {
    return rootState_subState == sendaction_3;
}

inline bool Detektor::Jazda_IN() const {
    return rootState_subState == Jazda;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Detektor.h
*********************************************************************/
