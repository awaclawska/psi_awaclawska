/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Ustawienia
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.h
*********************************************************************/

#ifndef Ustawienia_H
#define Ustawienia_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## link itsZmywarka
#include "Zmywarka.h"
//## package Default

//## class Ustawienia
class Ustawienia : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedUstawienia;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Ustawienia(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Ustawienia();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Zmywarka* getItsZmywarka() const;
    
    //## auto_generated
    Zmywarka* getItsZmywarka_1() const;
    
    //## auto_generated
    void setItsZmywarka_1(Zmywarka* p_Zmywarka);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Zmywarka itsZmywarka;		//## link itsZmywarka
    
    Zmywarka* itsZmywarka_1;		//## link itsZmywarka_1
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsZmywarka_1(Zmywarka* p_Zmywarka);
    
    //## auto_generated
    void _setItsZmywarka_1(Zmywarka* p_Zmywarka);
    
    //## auto_generated
    void _clearItsZmywarka_1();
    
    //## auto_generated
    virtual void destroy();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedUstawienia : virtual public AOMInstance {
    DECLARE_META(Ustawienia, OMAnimatedUstawienia)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.h
*********************************************************************/
