/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Ustawienia
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Ustawienia.h"
//#[ ignore
#define Default_Ustawienia_Ustawienia_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Ustawienia
Ustawienia::Ustawienia(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Ustawienia, Ustawienia(), 0, Default_Ustawienia_Ustawienia_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsZmywarka.setShouldDelete(false);
        }
    }
    itsZmywarka_1 = NULL;
    initRelations();
}

Ustawienia::~Ustawienia() {
    NOTIFY_DESTRUCTOR(~Ustawienia, true);
    cleanUpRelations();
}

Zmywarka* Ustawienia::getItsZmywarka() const {
    return (Zmywarka*) &itsZmywarka;
}

Zmywarka* Ustawienia::getItsZmywarka_1() const {
    return itsZmywarka_1;
}

void Ustawienia::setItsZmywarka_1(Zmywarka* p_Zmywarka) {
    if(p_Zmywarka != NULL)
        {
            p_Zmywarka->_addItsUstawienia_1(this);
        }
    _setItsZmywarka_1(p_Zmywarka);
}

bool Ustawienia::startBehavior() {
    bool done = true;
    done &= itsZmywarka.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void Ustawienia::initRelations() {
    itsZmywarka._setItsUstawienia(this);
}

void Ustawienia::cleanUpRelations() {
    if(itsZmywarka_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsZmywarka_1");
            Zmywarka* current = itsZmywarka_1;
            if(current != NULL)
                {
                    current->_removeItsUstawienia_1(this);
                }
            itsZmywarka_1 = NULL;
        }
}

void Ustawienia::__setItsZmywarka_1(Zmywarka* p_Zmywarka) {
    itsZmywarka_1 = p_Zmywarka;
    if(p_Zmywarka != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsZmywarka_1", p_Zmywarka, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsZmywarka_1");
        }
}

void Ustawienia::_setItsZmywarka_1(Zmywarka* p_Zmywarka) {
    if(itsZmywarka_1 != NULL)
        {
            itsZmywarka_1->_removeItsUstawienia_1(this);
        }
    __setItsZmywarka_1(p_Zmywarka);
}

void Ustawienia::_clearItsZmywarka_1() {
    NOTIFY_RELATION_CLEARED("itsZmywarka_1");
    itsZmywarka_1 = NULL;
}

void Ustawienia::destroy() {
    itsZmywarka.destroy();
    OMReactive::destroy();
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedUstawienia::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsZmywarka", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsZmywarka);
    aomsRelations->addRelation("itsZmywarka_1", false, true);
    if(myReal->itsZmywarka_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsZmywarka_1);
        }
}
//#]

IMPLEMENT_REACTIVE_META_SIMPLE_P(Ustawienia, Default, Default, false, OMAnimatedUstawienia)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.cpp
*********************************************************************/
