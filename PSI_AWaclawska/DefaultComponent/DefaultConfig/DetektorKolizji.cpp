/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: DetektorKolizji
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/DetektorKolizji.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "DetektorKolizji.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_DetektorKolizji_DetektorKolizji_SERIALIZE OM_NO_OP

#define Default_DetektorKolizji_czytajUstawienia_SERIALIZE OM_NO_OP

#define Default_DetektorKolizji_zapiszUstawienia_SERIALIZE aomsmethod->addAttribute("nastawy", UNKNOWN2STRING(nastawy));

#define Default_DetektorKolizji_zerujIloscImpulsow_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class DetektorKolizji
DetektorKolizji::DetektorKolizji(IOxfActive* theActiveContext) : iloscImpulsowOdebranych(0), iloscImpulsowWyslanych(0) {
    NOTIFY_ACTIVE_CONSTRUCTOR(DetektorKolizji, DetektorKolizji(), 0, Default_DetektorKolizji_DetektorKolizji_SERIALIZE);
    setActiveContext(this, true);
    itsSterownik = NULL;
    initStatechart();
}

DetektorKolizji::~DetektorKolizji() {
    NOTIFY_DESTRUCTOR(~DetektorKolizji, false);
    cleanUpRelations();
}

std::string DetektorKolizji::czytajUstawienia() {
    NOTIFY_OPERATION(czytajUstawienia, czytajUstawienia(), 0, Default_DetektorKolizji_czytajUstawienia_SERIALIZE);
    //#[ operation czytajUstawienia()
    //#]
}

bool DetektorKolizji::zapiszUstawienia(std::string nastawy) {
    NOTIFY_OPERATION(zapiszUstawienia, zapiszUstawienia(std::string), 1, Default_DetektorKolizji_zapiszUstawienia_SERIALIZE);
    //#[ operation zapiszUstawienia(std::string)
    //#]
}

void DetektorKolizji::zerujIloscImpulsow() {
    NOTIFY_OPERATION(zerujIloscImpulsow, zerujIloscImpulsow(), 0, Default_DetektorKolizji_zerujIloscImpulsow_SERIALIZE);
    //#[ operation zerujIloscImpulsow()
    iloscImpulsowWyslanych = 0;
    iloscImpulsowOdebranych = 0;
    //#]
}

Sterownik* DetektorKolizji::getItsSterownik() const {
    return itsSterownik;
}

void DetektorKolizji::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool DetektorKolizji::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void DetektorKolizji::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void DetektorKolizji::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

int DetektorKolizji::getIloscImpulsowOdebranych() const {
    return iloscImpulsowOdebranych;
}

void DetektorKolizji::setIloscImpulsowOdebranych(int p_iloscImpulsowOdebranych) {
    iloscImpulsowOdebranych = p_iloscImpulsowOdebranych;
}

int DetektorKolizji::getIloscImpulsowWyslanych() const {
    return iloscImpulsowWyslanych;
}

void DetektorKolizji::setIloscImpulsowWyslanych(int p_iloscImpulsowWyslanych) {
    iloscImpulsowWyslanych = p_iloscImpulsowWyslanych;
}

Stany DetektorKolizji::getKierunek() const {
    return kierunek;
}

void DetektorKolizji::setKierunek(Stany p_kierunek) {
    kierunek = p_kierunek;
}

void DetektorKolizji::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void DetektorKolizji::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void DetektorKolizji::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

void DetektorKolizji::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus DetektorKolizji::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(evStart_Default_id))
                {
                    OMSETPARAMS(evStart);
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda");
                    rootState_subState = Jazda;
                    rootState_active = Jazda;
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Jazda
        case Jazda:
        {
            if(IS_EVENT_TYPE_OF(evStop_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    NOTIFY_STATE_EXITED("ROOT.Jazda");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
                    rootState_subState = Wylaczony;
                    rootState_active = Wylaczony;
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evSprawdzKolizje_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("2");
                    NOTIFY_STATE_EXITED("ROOT.Jazda");
                    NOTIFY_STATE_ENTERED("ROOT.SprawdzanieKolizji");
                    pushNullTransition();
                    rootState_subState = SprawdzanieKolizji;
                    rootState_active = SprawdzanieKolizji;
                    NOTIFY_TRANSITION_TERMINATED("2");
                    res = eventConsumed;
                }
            
        }
        break;
        // State SprawdzanieKolizji
        case SprawdzanieKolizji:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 3 
                    if((iloscImpulsowWyslanych - iloscImpulsowOdebranych) > 3)
                        {
                            NOTIFY_TRANSITION_STARTED("3");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.SprawdzanieKolizji");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_3");
                            pushNullTransition();
                            rootState_subState = sendaction_3;
                            rootState_active = sendaction_3;
                            //#[ state sendaction_3.(Entry) 
                            itsSterownik->GEN(evKolizja);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("3");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_3
        case sendaction_3:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("4");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_3");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda");
                    rootState_subState = Jazda;
                    rootState_active = Jazda;
                    NOTIFY_TRANSITION_TERMINATED("4");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedDetektorKolizji::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("iloscImpulsowOdebranych", x2String(myReal->iloscImpulsowOdebranych));
    aomsAttributes->addAttribute("iloscImpulsowWyslanych", x2String(myReal->iloscImpulsowWyslanych));
    aomsAttributes->addAttribute("kierunek", x2String((int)myReal->kierunek));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedDetektorKolizji::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedDetektorKolizji::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case DetektorKolizji::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case DetektorKolizji::Jazda:
        {
            Jazda_serializeStates(aomsState);
        }
        break;
        case DetektorKolizji::SprawdzanieKolizji:
        {
            SprawdzanieKolizji_serializeStates(aomsState);
        }
        break;
        case DetektorKolizji::sendaction_3:
        {
            sendaction_3_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedDetektorKolizji::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedDetektorKolizji::SprawdzanieKolizji_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.SprawdzanieKolizji");
}

void OMAnimatedDetektorKolizji::sendaction_3_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_3");
}

void OMAnimatedDetektorKolizji::Jazda_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(DetektorKolizji, Default, false, Modul, OMAnimatedModul, OMAnimatedDetektorKolizji)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/DetektorKolizji.cpp
*********************************************************************/
