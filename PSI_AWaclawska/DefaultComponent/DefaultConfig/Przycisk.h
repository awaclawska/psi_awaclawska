/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Przycisk
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Przycisk.h
*********************************************************************/

#ifndef Przycisk_H
#define Przycisk_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Przycisk
#include "Modul.h"
//## link itsSterownik
#include "Sterownik.h"
//## link itsOdbiornik
class Odbiornik;

//## package Default

//## class Przycisk
class Przycisk : public OMThread, public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedPrzycisk;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    ////    Operations    ////
    
    //## operation czytajUstawienia()
    virtual std::string czytajUstawienia();
    
    //## operation zapiszUstawienia(std::string)
    virtual bool zapiszUstawienia(std::string nastawy);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Odbiornik* getItsOdbiornik() const;
    
    //## auto_generated
    void setItsOdbiornik(Odbiornik* p_Odbiornik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);
    
    ////    Relations and components    ////
    
    Odbiornik* itsOdbiornik;		//## link itsOdbiornik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsOdbiornik(Odbiornik* p_Odbiornik);
    
    //## auto_generated
    void _setItsOdbiornik(Odbiornik* p_Odbiornik);
    
    //## auto_generated
    void _clearItsOdbiornik();
    
    ////    Framework    ////
    
    //## auto_generated
    Przycisk(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Przycisk();
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    Sterownik* getItsSterownik_1() const;
    
    //## auto_generated
    void setItsSterownik_1(Sterownik* p_Sterownik);

protected :

    //## auto_generated
    void initRelations();
    
    Sterownik itsSterownik;		//## link itsSterownik
    
    Sterownik* itsSterownik_1;		//## link itsSterownik_1

public :

    //## auto_generated
    virtual void destroy();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // sendaction_2:
    //## statechart_method
    inline bool sendaction_2_IN() const;
    
    // Oczekiwanie:
    //## statechart_method
    inline bool Oczekiwanie_IN() const;

protected :

//#[ ignore
    enum Przycisk_Enum {
        OMNonState = 0,
        Wylaczony = 1,
        sendaction_2 = 2,
        Oczekiwanie = 3
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    IOxfTimeout* rootState_timeout;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedPrzycisk : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Przycisk, OMAnimatedPrzycisk)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_2_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Oczekiwanie_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Przycisk::rootState_IN() const {
    return true;
}

inline bool Przycisk::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool Przycisk::sendaction_2_IN() const {
    return rootState_subState == sendaction_2;
}

inline bool Przycisk::Oczekiwanie_IN() const {
    return rootState_subState == Oczekiwanie;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Przycisk.h
*********************************************************************/
