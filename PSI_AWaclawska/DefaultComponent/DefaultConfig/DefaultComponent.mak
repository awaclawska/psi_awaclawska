
############# Target type (Debug/Release) ##################
############################################################
CPPCompileDebug=-g
CPPCompileRelease=-O
LinkDebug=-g
LinkRelease=-O

CleanupFlagForSimulink=
SIMULINK_CONFIG=False
ifeq ($(SIMULINK_CONFIG),True)
CleanupFlagForSimulink=-DOM_WITH_CLEANUP
endif

ConfigurationCPPCompileSwitches=   $(INCLUDE_QUALIFIER). $(INCLUDE_QUALIFIER)$(OMROOT) $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/oxf $(DEFINE_QUALIFIER)CYGWIN $(INST_FLAGS) $(INCLUDE_PATH) $(INST_INCLUDES) -Wno-write-strings $(CPPCompileDebug) -c  $(CleanupFlagForSimulink)
ConfigurationCCCompileSwitches=$(INCLUDE_PATH) -c 

#########################################
###### Predefined macros ################
RM=/bin/rm -rf
INCLUDE_QUALIFIER=-I
DEFINE_QUALIFIER=-D
CC=g++
LIB_CMD=ar
LINK_CMD=g++
LIB_FLAGS=rvu
LINK_FLAGS= $(LinkDebug)   

#########################################
####### Context macros ##################

FLAGSFILE=
RULESFILE=
OMROOT="C:/ProgramData/IBM/Rational/Rhapsody/8.3.1/Share"
RHPROOT="C:/Program Files/IBM/Rational/Rhapsody/8.3.1"

CPP_EXT=.cpp
H_EXT=.h
OBJ_EXT=.o
EXE_EXT=.exe
LIB_EXT=.a

INSTRUMENTATION=Animation

TIME_MODEL=RealTime

TARGET_TYPE=Executable

TARGET_NAME=DefaultComponent

all : $(TARGET_NAME)$(EXE_EXT) DefaultComponent.mak

TARGET_MAIN=MainDefaultComponent

LIBS=

INCLUDE_PATH= \
  $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/osconfig/Cygwin

ADDITIONAL_OBJS=

OBJS= \
  Przycisk.o \
  Odbiornik.o \
  Sterownik.o \
  Naped.o \
  Detektor.o \
  Lampa.o \
  Zmywarka.o \
  Modul.o \
  Ustawienia.o \
  Default.o




#########################################
####### Predefined macros ###############
$(OBJS) : $(INST_LIBS) $(OXF_LIBS)

ifeq ($(INSTRUMENTATION),Animation)

INST_FLAGS=$(DEFINE_QUALIFIER)OMANIMATOR $(DEFINE_QUALIFIER)__USE_W32_SOCKETS 
INST_INCLUDES=$(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/aom $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/tom
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwinaomanim$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinoxsiminst$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxfinst$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinomcomappl$(LIB_EXT)
SOCK_LIB=-lws2_32

else
ifeq ($(INSTRUMENTATION),Tracing)

INST_FLAGS=$(DEFINE_QUALIFIER)OMTRACER 
INST_INCLUDES=$(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/aom $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/tom
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwintomtrace$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinaomtrace$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinoxsiminst$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxfinst$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinomcomappl$(LIB_EXT)
SOCK_LIB=-lws2_32

else
ifeq ($(INSTRUMENTATION),None)

INST_FLAGS= 
INST_INCLUDES=
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxsim$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxf$(LIB_EXT)
SOCK_LIB=-lws2_32

else
	@echo An invalid Instrumentation $(INSTRUMENTATION) is specified.
	exit
endif
endif
endif

.SUFFIXES: $(CPP_EXT)

#####################################################################
##################### Context dependencies and commands #############






Przycisk.o : Przycisk.cpp Przycisk.h    Default.h Odbiornik.h Sterownik.h Modul.h 
	@echo Compiling Przycisk.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Przycisk.o Przycisk.cpp




Odbiornik.o : Odbiornik.cpp Odbiornik.h    Default.h Sterownik.h Przycisk.h Modul.h 
	@echo Compiling Odbiornik.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Odbiornik.o Odbiornik.cpp




Sterownik.o : Sterownik.cpp Sterownik.h    Default.h Zmywarka.h Lampa.h Detektor.h Odbiornik.h Naped.h Przycisk.h Modul.h 
	@echo Compiling Sterownik.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Sterownik.o Sterownik.cpp




Naped.o : Naped.cpp Naped.h    Default.h Sterownik.h Modul.h 
	@echo Compiling Naped.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Naped.o Naped.cpp




Detektor.o : Detektor.cpp Detektor.h    Default.h Sterownik.h Modul.h 
	@echo Compiling Detektor.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Detektor.o Detektor.cpp




Lampa.o : Lampa.cpp Lampa.h    Default.h Sterownik.h Modul.h 
	@echo Compiling Lampa.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Lampa.o Lampa.cpp




Zmywarka.o : Zmywarka.cpp Zmywarka.h    Default.h Sterownik.h Ustawienia.h Modul.h 
	@echo Compiling Zmywarka.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Zmywarka.o Zmywarka.cpp




Modul.o : Modul.cpp Modul.h    Default.h 
	@echo Compiling Modul.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Modul.o Modul.cpp




Ustawienia.o : Ustawienia.cpp Ustawienia.h    Default.h Zmywarka.h Modul.h 
	@echo Compiling Ustawienia.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Ustawienia.o Ustawienia.cpp




Default.o : Default.cpp Default.h    Przycisk.h Odbiornik.h Sterownik.h Naped.h Detektor.h Lampa.h Zmywarka.h Modul.h Ustawienia.h 
	@echo Compiling Default.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Default.o Default.cpp







$(TARGET_MAIN)$(OBJ_EXT) : $(TARGET_MAIN)$(CPP_EXT) $(OBJS)
	@echo Compiling $(TARGET_MAIN)$(CPP_EXT)
	@$(CC) $(ConfigurationCPPCompileSwitches) -o  $(TARGET_MAIN)$(OBJ_EXT) $(TARGET_MAIN)$(CPP_EXT)

####################################################################
############## Predefined Instructions #############################
$(TARGET_NAME)$(EXE_EXT): $(OBJS) $(ADDITIONAL_OBJS) $(TARGET_MAIN)$(OBJ_EXT) DefaultComponent.mak
	@echo Linking $(TARGET_NAME)$(EXE_EXT)
	@$(LINK_CMD)  $(TARGET_MAIN)$(OBJ_EXT) $(OBJS) $(ADDITIONAL_OBJS) \
	$(LIBS) \
	$(OXF_LIBS) \
	$(INST_LIBS) \
	$(OXF_LIBS) \
	$(INST_LIBS) \
	$(SOCK_LIB) \
	$(LINK_FLAGS) -o $(TARGET_NAME)$(EXE_EXT)

$(TARGET_NAME)$(LIB_EXT) : $(OBJS) $(ADDITIONAL_OBJS) DefaultComponent.mak
	@echo Building library $@
	@$(LIB_CMD) $(LIB_FLAGS) $(TARGET_NAME)$(LIB_EXT) $(OBJS) $(ADDITIONAL_OBJS)



clean:
	@echo Cleanup
	$(RM) Przycisk.o
	$(RM) Odbiornik.o
	$(RM) Sterownik.o
	$(RM) Naped.o
	$(RM) Detektor.o
	$(RM) Lampa.o
	$(RM) Zmywarka.o
	$(RM) Modul.o
	$(RM) Ustawienia.o
	$(RM) Default.o
	$(RM) $(TARGET_MAIN)$(OBJ_EXT) $(ADDITIONAL_OBJS)
	$(RM) $(TARGET_NAME)$(LIB_EXT)
	$(RM) $(TARGET_NAME)$(EXE_EXT)

