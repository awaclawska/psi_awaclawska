/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Naped
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Naped.h
*********************************************************************/

#ifndef Naped_H
#define Naped_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Naped
#include "Modul.h"
//## link itsSterownik
class Sterownik;

//## package Default

//## class Naped
class Naped : public OMThread, public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedNaped;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Naped(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Naped();
    
    ////    Operations    ////
    
    //## operation czytajUstawienia()
    virtual std::string czytajUstawienia();
    
    //## operation zapiszUstawienia(std::string)
    virtual bool zapiszUstawienia(std::string nastawy);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);

private :

    //## auto_generated
    Stany getKierunek() const;
    
    //## auto_generated
    void setKierunek(Stany p_kierunek);
    
    //## auto_generated
    int getOkres() const;
    
    //## auto_generated
    void setOkres(int p_okres);
    
    //## auto_generated
    float getPredkosc() const;
    
    //## auto_generated
    void setPredkosc(float p_predkosc);
    
    ////    Attributes    ////

protected :

    Stany kierunek;		//## attribute kierunek
    
    int okres;		//## attribute okres
    
    float predkosc;		//## attribute predkosc
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // sendaction_2:
    //## statechart_method
    inline bool sendaction_2_IN() const;
    
    // Jazda:
    //## statechart_method
    inline bool Jazda_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Naped_Enum {
        OMNonState = 0,
        Wylaczony = 1,
        sendaction_2 = 2,
        Jazda = 3
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    IOxfTimeout* rootState_timeout;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedNaped : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Naped, OMAnimatedNaped)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_2_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Jazda_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Naped::rootState_IN() const {
    return true;
}

inline bool Naped::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool Naped::sendaction_2_IN() const {
    return rootState_subState == sendaction_2;
}

inline bool Naped::Jazda_IN() const {
    return rootState_subState == Jazda;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Naped.h
*********************************************************************/
