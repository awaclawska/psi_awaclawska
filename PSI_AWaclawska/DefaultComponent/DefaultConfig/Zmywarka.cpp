/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Zmywarka
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Zmywarka.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Zmywarka.h"
//## link itsUstawienia
#include "Ustawienia.h"
//#[ ignore
#define Default_Zmywarka_Zmywarka_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Zmywarka
Zmywarka::Zmywarka(IOxfActive* theActiveContext) {
    NOTIFY_ACTIVE_CONSTRUCTOR(Zmywarka, Zmywarka(), 0, Default_Zmywarka_Zmywarka_SERIALIZE);
    setActiveContext(this, true);
    {
        {
            itsSterownik.setShouldDelete(false);
        }
    }
    itsUstawienia = NULL;
    initRelations();
    initStatechart();
}

Zmywarka::~Zmywarka() {
    NOTIFY_DESTRUCTOR(~Zmywarka, true);
    cleanUpRelations();
}

Sterownik* Zmywarka::getItsSterownik() const {
    return (Sterownik*) &itsSterownik;
}

Ustawienia* Zmywarka::getItsUstawienia() const {
    return itsUstawienia;
}

void Zmywarka::setItsUstawienia(Ustawienia* p_Ustawienia) {
    _setItsUstawienia(p_Ustawienia);
}

OMIterator<Ustawienia*> Zmywarka::getItsUstawienia_1() const {
    OMIterator<Ustawienia*> iter(itsUstawienia_1);
    return iter;
}

Ustawienia* Zmywarka::newItsUstawienia_1() {
    Ustawienia* newUstawienia = new Ustawienia(getActiveContext());
    newUstawienia->_setItsZmywarka_1(this);
    itsUstawienia_1.add(newUstawienia);
    NOTIFY_RELATION_ITEM_ADDED("itsUstawienia_1", newUstawienia, true, false);
    return newUstawienia;
}

void Zmywarka::deleteItsUstawienia_1(Ustawienia* p_Ustawienia) {
    p_Ustawienia->_setItsZmywarka_1(NULL);
    itsUstawienia_1.remove(p_Ustawienia);
    NOTIFY_RELATION_ITEM_REMOVED("itsUstawienia_1", p_Ustawienia);
    p_Ustawienia->destroy();
}

bool Zmywarka::startBehavior() {
    bool done = true;
    done &= itsSterownik.startBehavior();
    {
        OMIterator<Ustawienia*> iter(itsUstawienia_1);
        while (*iter){
            done &= (*iter)->startBehavior();
            iter++;
        }
    }
    done &= OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void Zmywarka::initRelations() {
    itsSterownik._setItsZmywarka(this);
}

void Zmywarka::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Zmywarka::cleanUpRelations() {
    itsUstawienia_1.removeAll();
    if(itsUstawienia != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsUstawienia");
            itsUstawienia = NULL;
        }
}

int Zmywarka::getPilotAktywny() const {
    return pilotAktywny;
}

void Zmywarka::setPilotAktywny(int p_pilotAktywny) {
    pilotAktywny = p_pilotAktywny;
}

int Zmywarka::getPilotAktywowany() const {
    return pilotAktywowany;
}

void Zmywarka::setPilotAktywowany(int p_pilotAktywowany) {
    pilotAktywowany = p_pilotAktywowany;
}

void Zmywarka::__setItsUstawienia(Ustawienia* p_Ustawienia) {
    itsUstawienia = p_Ustawienia;
    if(p_Ustawienia != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsUstawienia", p_Ustawienia, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsUstawienia");
        }
}

void Zmywarka::_setItsUstawienia(Ustawienia* p_Ustawienia) {
    __setItsUstawienia(p_Ustawienia);
}

void Zmywarka::_clearItsUstawienia() {
    NOTIFY_RELATION_CLEARED("itsUstawienia");
    itsUstawienia = NULL;
}

void Zmywarka::_addItsUstawienia_1(Ustawienia* p_Ustawienia) {
    if(p_Ustawienia != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsUstawienia_1", p_Ustawienia, false, false);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsUstawienia_1");
        }
    itsUstawienia_1.add(p_Ustawienia);
}

void Zmywarka::_removeItsUstawienia_1(Ustawienia* p_Ustawienia) {
    NOTIFY_RELATION_ITEM_REMOVED("itsUstawienia_1", p_Ustawienia);
    itsUstawienia_1.remove(p_Ustawienia);
}

void Zmywarka::destroy() {
    itsSterownik.destroy();
    {
        OMIterator<Ustawienia*> iter(itsUstawienia_1);
        while (*iter){
            (*iter)->destroy();
            iter++;
        }
    }
    OMReactive::destroy();
}

void Zmywarka::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
        rootState_subState = Oczekiwanie;
        rootState_active = Oczekiwanie;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Zmywarka::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Oczekiwanie
        case Oczekiwanie:
        {
            res = Oczekiwanie_handleEvent();
        }
        break;
        // State sendaction_1
        case sendaction_1:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("2");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_1");
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    NOTIFY_TRANSITION_TERMINATED("2");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_2
        case sendaction_2:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("7");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_2");
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    NOTIFY_TRANSITION_TERMINATED("7");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_3
        case sendaction_3:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("6");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_3");
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    NOTIFY_TRANSITION_TERMINATED("6");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_4
        case sendaction_4:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("8");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_4");
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    NOTIFY_TRANSITION_TERMINATED("8");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

IOxfReactive::TakeEventStatus Zmywarka::Oczekiwanie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evKolizja_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("5");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.sendaction_3");
            pushNullTransition();
            rootState_subState = sendaction_3;
            rootState_active = sendaction_3;
            //#[ state sendaction_3.(Entry) 
            itsSterownik.GEN(evKolizja);
            //#]
            NOTIFY_TRANSITION_TERMINATED("5");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evMigaj_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("4");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.sendaction_2");
            pushNullTransition();
            rootState_subState = sendaction_2;
            rootState_active = sendaction_2;
            //#[ state sendaction_2.(Entry) 
            itsSterownik.GEN(evMigaj);
            //#]
            NOTIFY_TRANSITION_TERMINATED("4");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evPrzycisk_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("3");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.sendaction_4");
            pushNullTransition();
            rootState_subState = sendaction_4;
            rootState_active = sendaction_4;
            //#[ state sendaction_4.(Entry) 
            itsSterownik.GEN(evPrzycisk);
            //#]
            NOTIFY_TRANSITION_TERMINATED("3");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evZamkniete_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("1");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.sendaction_1");
            pushNullTransition();
            rootState_subState = sendaction_1;
            rootState_active = sendaction_1;
            //#[ state sendaction_1.(Entry) 
            itsSterownik.GEN(evZamkniete);
            //#]
            NOTIFY_TRANSITION_TERMINATED("1");
            res = eventConsumed;
        }
    
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedZmywarka::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("pilotAktywny", x2String(myReal->pilotAktywny));
    aomsAttributes->addAttribute("pilotAktywowany", x2String(myReal->pilotAktywowany));
}

void OMAnimatedZmywarka::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSterownik);
    aomsRelations->addRelation("itsUstawienia", false, true);
    if(myReal->itsUstawienia)
        {
            aomsRelations->ADD_ITEM(myReal->itsUstawienia);
        }
    aomsRelations->addRelation("itsUstawienia_1", true, false);
    {
        OMIterator<Ustawienia*> iter(myReal->itsUstawienia_1);
        while (*iter){
            aomsRelations->ADD_ITEM(*iter);
            iter++;
        }
    }
}

void OMAnimatedZmywarka::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Zmywarka::Oczekiwanie:
        {
            Oczekiwanie_serializeStates(aomsState);
        }
        break;
        case Zmywarka::sendaction_1:
        {
            sendaction_1_serializeStates(aomsState);
        }
        break;
        case Zmywarka::sendaction_2:
        {
            sendaction_2_serializeStates(aomsState);
        }
        break;
        case Zmywarka::sendaction_3:
        {
            sendaction_3_serializeStates(aomsState);
        }
        break;
        case Zmywarka::sendaction_4:
        {
            sendaction_4_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedZmywarka::sendaction_4_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_4");
}

void OMAnimatedZmywarka::sendaction_3_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_3");
}

void OMAnimatedZmywarka::sendaction_2_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_2");
}

void OMAnimatedZmywarka::sendaction_1_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_1");
}

void OMAnimatedZmywarka::Oczekiwanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Oczekiwanie");
}
//#]

IMPLEMENT_REACTIVE_META_P(Zmywarka, Default, Default, false, OMAnimatedZmywarka)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Zmywarka.cpp
*********************************************************************/
