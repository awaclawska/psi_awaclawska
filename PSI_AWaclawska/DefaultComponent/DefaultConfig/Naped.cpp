/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Naped
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Naped.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Naped.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_Naped_Naped_SERIALIZE OM_NO_OP

#define Default_Naped_czytajUstawienia_SERIALIZE OM_NO_OP

#define Default_Naped_zapiszUstawienia_SERIALIZE aomsmethod->addAttribute("nastawy", UNKNOWN2STRING(nastawy));
//#]

//## package Default

//## class Naped
Naped::Naped(IOxfActive* theActiveContext) : okres(250), predkosc(1.) {
    NOTIFY_ACTIVE_CONSTRUCTOR(Naped, Naped(), 0, Default_Naped_Naped_SERIALIZE);
    setActiveContext(this, true);
    itsSterownik = NULL;
    initStatechart();
}

Naped::~Naped() {
    NOTIFY_DESTRUCTOR(~Naped, false);
    cleanUpRelations();
    cancelTimeouts();
}

std::string Naped::czytajUstawienia() {
    NOTIFY_OPERATION(czytajUstawienia, czytajUstawienia(), 0, Default_Naped_czytajUstawienia_SERIALIZE);
    //#[ operation czytajUstawienia()
    //#]
}

bool Naped::zapiszUstawienia(std::string nastawy) {
    NOTIFY_OPERATION(zapiszUstawienia, zapiszUstawienia(std::string), 1, Default_Naped_zapiszUstawienia_SERIALIZE);
    //#[ operation zapiszUstawienia(std::string)
    //#]
}

Sterownik* Naped::getItsSterownik() const {
    return itsSterownik;
}

void Naped::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Naped::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void Naped::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    rootState_timeout = NULL;
}

void Naped::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void Naped::cancelTimeouts() {
    cancel(rootState_timeout);
}

bool Naped::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(rootState_timeout == arg)
        {
            rootState_timeout = NULL;
            res = true;
        }
    return res;
}

Stany Naped::getKierunek() const {
    return kierunek;
}

void Naped::setKierunek(Stany p_kierunek) {
    kierunek = p_kierunek;
}

int Naped::getOkres() const {
    return okres;
}

void Naped::setOkres(int p_okres) {
    okres = p_okres;
}

float Naped::getPredkosc() const {
    return predkosc;
}

void Naped::setPredkosc(float p_predkosc) {
    predkosc = p_predkosc;
}

void Naped::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Naped::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Naped::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

void Naped::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Naped::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(evStart_Default_id))
                {
                    OMSETPARAMS(evStart);
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    //#[ transition 1 
                    kierunek = params->stan;
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.Jazda");
                    rootState_subState = Jazda;
                    rootState_active = Jazda;
                    rootState_timeout = scheduleTimeout(okres, "ROOT.Jazda");
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Jazda
        case Jazda:
        {
            if(IS_EVENT_TYPE_OF(evStop_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("4");
                    cancel(rootState_timeout);
                    NOTIFY_STATE_EXITED("ROOT.Jazda");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
                    rootState_subState = Wylaczony;
                    rootState_active = Wylaczony;
                    NOTIFY_TRANSITION_TERMINATED("4");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("2");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.Jazda");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_2");
                            pushNullTransition();
                            rootState_subState = sendaction_2;
                            rootState_active = sendaction_2;
                            //#[ state sendaction_2.(Entry) 
                            itsSterownik->GEN(evEtap(predkosc * okres * kierunek));
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("2");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_2
        case sendaction_2:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("3");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_2");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda");
                    rootState_subState = Jazda;
                    rootState_active = Jazda;
                    rootState_timeout = scheduleTimeout(okres, "ROOT.Jazda");
                    NOTIFY_TRANSITION_TERMINATED("3");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedNaped::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("okres", x2String(myReal->okres));
    aomsAttributes->addAttribute("predkosc", x2String(myReal->predkosc));
    aomsAttributes->addAttribute("kierunek", x2String((int)myReal->kierunek));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedNaped::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedNaped::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Naped::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case Naped::Jazda:
        {
            Jazda_serializeStates(aomsState);
        }
        break;
        case Naped::sendaction_2:
        {
            sendaction_2_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedNaped::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedNaped::sendaction_2_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_2");
}

void OMAnimatedNaped::Jazda_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Naped, Default, false, Modul, OMAnimatedModul, OMAnimatedNaped)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Naped.cpp
*********************************************************************/
