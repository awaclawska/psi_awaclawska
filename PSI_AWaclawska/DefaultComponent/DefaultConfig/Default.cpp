/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Default.h"
//## auto_generated
#include "Detektor.h"
//## auto_generated
#include "Lampa.h"
//## auto_generated
#include "Modul.h"
//## auto_generated
#include "Naped.h"
//## auto_generated
#include "Odbiornik.h"
//## auto_generated
#include "Przycisk.h"
//## auto_generated
#include "Sterownik.h"
//## auto_generated
#include "Ustawienia.h"
//## auto_generated
#include "Zmywarka.h"
//#[ ignore
#define eventmessage_0_SERIALIZE OM_NO_OP

#define eventmessage_0_UNSERIALIZE OM_NO_OP

#define eventmessage_0_CONSTRUCTOR eventmessage_0()

#define evPrzycisk_SERIALIZE OM_NO_OP

#define evPrzycisk_UNSERIALIZE OM_NO_OP

#define evPrzycisk_CONSTRUCTOR evPrzycisk()

#define evValidate_SERIALIZE OM_NO_OP

#define evValidate_UNSERIALIZE OM_NO_OP

#define evValidate_CONSTRUCTOR evValidate()

#define evDekoduj_SERIALIZE OM_NO_OP

#define evDekoduj_UNSERIALIZE OM_NO_OP

#define evDekoduj_CONSTRUCTOR evDekoduj()

#define evStop_SERIALIZE OM_NO_OP

#define evStop_UNSERIALIZE OM_NO_OP

#define evStop_CONSTRUCTOR evStop()

#define evStart_SERIALIZE OMADD_SER(stan, x2String((int)myEvent->stan))

#define evStart_UNSERIALIZE OMADD_UNSER(int, stan, OMDestructiveString2X)

#define evStart_CONSTRUCTOR evStart(stan)

#define evEtap_SERIALIZE OMADD_SER(droga, x2String(myEvent->droga))

#define evEtap_UNSERIALIZE OMADD_UNSER(float, droga, OMDestructiveString2X)

#define evEtap_CONSTRUCTOR evEtap(droga)

#define evMigaj_SERIALIZE OM_NO_OP

#define evMigaj_UNSERIALIZE OM_NO_OP

#define evMigaj_CONSTRUCTOR evMigaj()

#define evSprawdzKolizje_SERIALIZE OM_NO_OP

#define evSprawdzKolizje_UNSERIALIZE OM_NO_OP

#define evSprawdzKolizje_CONSTRUCTOR evSprawdzKolizje()

#define evKolizja_SERIALIZE OM_NO_OP

#define evKolizja_UNSERIALIZE OM_NO_OP

#define evKolizja_CONSTRUCTOR evKolizja()

#define evPauza_SERIALIZE OM_NO_OP

#define evPauza_UNSERIALIZE OM_NO_OP

#define evPauza_CONSTRUCTOR evPauza()

#define evZamkniete_SERIALIZE OM_NO_OP

#define evZamkniete_UNSERIALIZE OM_NO_OP

#define evZamkniete_CONSTRUCTOR evZamkniete()

#define evTestowanie_SERIALIZE OM_NO_OP

#define evTestowanie_UNSERIALIZE OM_NO_OP

#define evTestowanie_CONSTRUCTOR evTestowanie()

#define evZatrzymanie_SERIALIZE OM_NO_OP

#define evZatrzymanie_UNSERIALIZE OM_NO_OP

#define evZatrzymanie_CONSTRUCTOR evZatrzymanie()

#define evSuszenie_SERIALIZE OM_NO_OP

#define evSuszenie_UNSERIALIZE OM_NO_OP

#define evSuszenie_CONSTRUCTOR evSuszenie()
//#]

//## package Default


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

IMPLEMENT_META_PACKAGE(Default, Default)

static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}
#endif // _OMINSTRUMENT

//## event eventmessage_0()
eventmessage_0::eventmessage_0() {
    NOTIFY_EVENT_CONSTRUCTOR(eventmessage_0)
    setId(eventmessage_0_Default_id);
}

bool eventmessage_0::isTypeOf(const short id) const {
    return (eventmessage_0_Default_id==id);
}

IMPLEMENT_META_EVENT_P(eventmessage_0, Default, Default, eventmessage_0())

//## event evPrzycisk()
evPrzycisk::evPrzycisk() {
    NOTIFY_EVENT_CONSTRUCTOR(evPrzycisk)
    setId(evPrzycisk_Default_id);
}

bool evPrzycisk::isTypeOf(const short id) const {
    return (evPrzycisk_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evPrzycisk, Default, Default, evPrzycisk())

//## event evValidate()
evValidate::evValidate() {
    NOTIFY_EVENT_CONSTRUCTOR(evValidate)
    setId(evValidate_Default_id);
}

bool evValidate::isTypeOf(const short id) const {
    return (evValidate_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evValidate, Default, Default, evValidate())

//## event evDekoduj()
evDekoduj::evDekoduj() {
    NOTIFY_EVENT_CONSTRUCTOR(evDekoduj)
    setId(evDekoduj_Default_id);
}

bool evDekoduj::isTypeOf(const short id) const {
    return (evDekoduj_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evDekoduj, Default, Default, evDekoduj())

//## event evStop()
evStop::evStop() {
    NOTIFY_EVENT_CONSTRUCTOR(evStop)
    setId(evStop_Default_id);
}

bool evStop::isTypeOf(const short id) const {
    return (evStop_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStop, Default, Default, evStop())

//## event evStart(Stany)
//#[ ignore
evStart::evStart(int p_stan) : stan((Stany)p_stan) {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}
//#]

evStart::evStart() {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}

evStart::evStart(Stany p_stan) : stan(p_stan) {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}

bool evStart::isTypeOf(const short id) const {
    return (evStart_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStart, Default, Default, evStart(Stany))

//## event evEtap(float)
evEtap::evEtap() {
    NOTIFY_EVENT_CONSTRUCTOR(evEtap)
    setId(evEtap_Default_id);
}

evEtap::evEtap(float p_droga) : droga(p_droga) {
    NOTIFY_EVENT_CONSTRUCTOR(evEtap)
    setId(evEtap_Default_id);
}

bool evEtap::isTypeOf(const short id) const {
    return (evEtap_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evEtap, Default, Default, evEtap(float))

//## event evMigaj()
evMigaj::evMigaj() {
    NOTIFY_EVENT_CONSTRUCTOR(evMigaj)
    setId(evMigaj_Default_id);
}

bool evMigaj::isTypeOf(const short id) const {
    return (evMigaj_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evMigaj, Default, Default, evMigaj())

//## event evSprawdzKolizje()
evSprawdzKolizje::evSprawdzKolizje() {
    NOTIFY_EVENT_CONSTRUCTOR(evSprawdzKolizje)
    setId(evSprawdzKolizje_Default_id);
}

bool evSprawdzKolizje::isTypeOf(const short id) const {
    return (evSprawdzKolizje_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evSprawdzKolizje, Default, Default, evSprawdzKolizje())

//## event evKolizja()
evKolizja::evKolizja() {
    NOTIFY_EVENT_CONSTRUCTOR(evKolizja)
    setId(evKolizja_Default_id);
}

bool evKolizja::isTypeOf(const short id) const {
    return (evKolizja_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKolizja, Default, Default, evKolizja())

//## event evPauza()
evPauza::evPauza() {
    NOTIFY_EVENT_CONSTRUCTOR(evPauza)
    setId(evPauza_Default_id);
}

bool evPauza::isTypeOf(const short id) const {
    return (evPauza_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evPauza, Default, Default, evPauza())

//## event evZamkniete()
evZamkniete::evZamkniete() {
    NOTIFY_EVENT_CONSTRUCTOR(evZamkniete)
    setId(evZamkniete_Default_id);
}

bool evZamkniete::isTypeOf(const short id) const {
    return (evZamkniete_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evZamkniete, Default, Default, evZamkniete())

//## event evTestowanie()
evTestowanie::evTestowanie() {
    NOTIFY_EVENT_CONSTRUCTOR(evTestowanie)
    setId(evTestowanie_Default_id);
}

bool evTestowanie::isTypeOf(const short id) const {
    return (evTestowanie_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evTestowanie, Default, Default, evTestowanie())

//## event evZatrzymanie()
evZatrzymanie::evZatrzymanie() {
    NOTIFY_EVENT_CONSTRUCTOR(evZatrzymanie)
    setId(evZatrzymanie_Default_id);
}

bool evZatrzymanie::isTypeOf(const short id) const {
    return (evZatrzymanie_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evZatrzymanie, Default, Default, evZatrzymanie())

//## event evSuszenie()
evSuszenie::evSuszenie() {
    NOTIFY_EVENT_CONSTRUCTOR(evSuszenie)
    setId(evSuszenie_Default_id);
}

bool evSuszenie::isTypeOf(const short id) const {
    return (evSuszenie_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evSuszenie, Default, Default, evSuszenie())

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/
