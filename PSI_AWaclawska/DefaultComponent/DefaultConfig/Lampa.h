/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Lampa
//!	Generated Date	: Sun, 20, Sep 2020  
	File Path	: DefaultComponent/DefaultConfig/Lampa.h
*********************************************************************/

#ifndef Lampa_H
#define Lampa_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Lampa
#include "Modul.h"
//## link itsSterownik
class Sterownik;

//## package Default

//## class Lampa
class Lampa : public OMThread, public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedLampa;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Lampa(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Lampa();
    
    ////    Operations    ////
    
    //## operation czytajUstawienia()
    virtual std::string czytajUstawienia();
    
    //## operation zapiszUstawienia(std::string)
    virtual bool zapiszUstawienia(std::string nastawy);
    
    ////    Additional operations    ////
    
    //## auto_generated
    void setSwieciCzerwony(bool p_swieciCzerwony);
    
    //## auto_generated
    void setSwieciZielony(bool p_swieciZielony);
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    float getDroga() const;
    
    //## auto_generated
    void setDroga(float p_droga);
    
    //## auto_generated
    Stany getKierunek() const;
    
    //## auto_generated
    void setKierunek(Stany p_kierunek);
    
    //## auto_generated
    bool getSwieciCzerwony() const;
    
    //## auto_generated
    bool getSwieciZielony() const;
    
    ////    Attributes    ////

protected :

    float droga;		//## attribute droga
    
    Stany kierunek;		//## attribute kierunek
    
    bool swieciCzerwony;		//## attribute swieciCzerwony
    
    bool swieciZielony;		//## attribute swieciZielony
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Zamykanie:
    //## statechart_method
    inline bool Zamykanie_IN() const;
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // Otwieranie:
    //## statechart_method
    inline bool Otwieranie_IN() const;
    
    // Jazda:
    //## statechart_method
    inline bool Jazda_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Lampa_Enum {
        OMNonState = 0,
        Zamykanie = 1,
        Wylaczony = 2,
        Otwieranie = 3,
        Jazda = 4
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedLampa : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Lampa, OMAnimatedLampa)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Zamykanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Otwieranie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Jazda_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Lampa::rootState_IN() const {
    return true;
}

inline bool Lampa::Zamykanie_IN() const {
    return rootState_subState == Zamykanie;
}

inline bool Lampa::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool Lampa::Otwieranie_IN() const {
    return rootState_subState == Otwieranie;
}

inline bool Lampa::Jazda_IN() const {
    return rootState_subState == Jazda;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Lampa.h
*********************************************************************/
